#ifndef MACRO_GUI_STR_H
#define MACRO_GUI_STR_H

#define ERROR_TYPE_STR "错误类型："
#define ERROR_CODE_STR "错误代码："
#define ERROR_STR "错误"
#define DISK_FULL_STR "磁盘已满"
#define FILE_EXIST_STR1 "文件已存在"
#define FILE_EXIST_STR2 "文件已存在，是否覆盖？"
#define YES_STR "是"
#define NO_STR "否"
#define CONFIRM_STR "确定"
#define WRITE_ERROR_STR "写入文件错误"
#define READ_ERROR_STR "读取文件错误"
#define CREATE_ERROR_STR "创建文件错误"
#define UNKNOWN_ERROR_STR "未知错误"
#define FILE_NOT_FOUND_STR "文件没发现"
#define COPYING_FILE_STR "有文件正在复制，是否关闭？"
#define IS_CLOSE_STR "是否关闭"
#define OPENDISK_ERROR_STR "打开磁盘错误"


#define DISK_STR "磁盘"
#define NATIVE_DISK "本地磁盘"
#define REMOVABLE_DISK "可移动磁盘"
#define EXT_DISK "EXT分区"
#define UNKNOWN_DISK "未知分区" 


#define FILE_NAME_STR "文件名"
#define FILE_SIZE_STR "大小"
#define COPY_STR "复制"

#endif // MACRO_GUI_STR_H