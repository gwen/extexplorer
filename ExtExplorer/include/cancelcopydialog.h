#ifndef CANCELCOPYDIALOG_H
#define CANCELCOPYDIALOG_H
#include <QtGui/QDialog>
#include <QFrame>
#include "type.h"
#include "ui_cancelcopy.h"
class CancelCopyDialog:public QDialog,public Ui_CancelCopyDialog
{
	Q_OBJECT
	friend class ErrorHandle;
private:
	CancelCopyDialog(QWidget* parent);
	void setCheckBox(bool ico);
	void setDefaultCheck();
	bool is_dir_enable;
	static Qt::CheckState default_ico;
	static Qt::CheckState default_ica;
	static Qt::CheckState default_ida;
	static Qt::CheckState default_idf;
	static Qt::CheckState default_idd;
private slots:
	void checkICA(int state);
	void checkIDD(int state);
	void checkIDA(int state);
	void setDefaultValue();
	void showOption();
};
#endif // CANCELCOPYDIALOG_H