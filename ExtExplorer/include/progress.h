#ifndef PROGRESS_H
#define PROGRESS_H
#include <QObject>
#include <QThread>
#include "type.h"
class QProgressBar;
class QLabel;
class QString;
class QFrame;
class ProgressReport;
class Progress:public QObject
{
	Q_OBJECT
public:
	Progress():copy_progress_bar(NULL),label_to_path(NULL),label_file_name(NULL),label_from_path(NULL){}
	void setWidgets(QProgressBar* bar,QLabel* to,QLabel* from,QLabel* name,QFrame* frame);
private slots:
	void start(const QString& from_path,const QString& to_path,uint32 file_type);
	void start();
	void end();
	void receiveProgress(const ProgressReport& pr);
private:
	QProgressBar* copy_progress_bar;
	QLabel* label_to_path;
	QLabel* label_from_path;
	QLabel* label_file_name;
	QFrame* frame_progress;
};
#endif // PROGRESS_H