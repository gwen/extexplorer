#ifndef REPORT_H
#define REPORT_H
#include "type.h"
#include <QString>
class ErrorReport
{
public:
	ErrorReport():error_type(0),error_code(0),extra_info(NULL){}
	ErrorReport(uint32 t,uint32 c,void* extra_info_=NULL):error_type(t),error_code(c),extra_info(extra_info_){}
	uint32 type() const{return error_type;}
	uint32 code() const{return error_code;}
	const void* extraInfo() const{return extra_info;}
	void setExtraInfo(void* info){extra_info=info;}
private:
	uint32 error_type;
	uint32 error_code;
	void* extra_info;
};
class ProgressReport
{
	friend class RequestHandle;
	friend class WatchThread;
public:
	uint32 progressType() const{return progress_type;}
	uint32 progressValue() const{return progress_value;}
	const uint16* fileName() const{return name;}
	uint32 fileNameLen() const{return name_length;}
	void setFileName(const QString& name){file_name=name;}
	const QString& progressFileName() const{return file_name;}
private:
	ProgressReport():progress_type(0),progress_value(0){}
	uint32 progress_type;
	uint32 progress_value;
	const uint16* name;
	uint32 name_length;
	QString file_name;
};

#endif // REPORT_H