#ifndef EVENTS_H
#define EVENTS_H
#include "type.h"
#include <QEvent>
#define EVENT_TYPE_COPY_START (QEvent::Type)(2001)
#define EVENT_TYPE_ALL_COPY_START (QEvent::Type)(2002)
#define EVENT_TYPE_ALL_COPY_END (QEvent::Type)(2003)
#define EVENT_TYPE_COPY_ERROR (QEvent::Type)(2004)
#define EVENT_TYPE_COPY_CANCEL (QEvent::Type)(2005)
#define EVENT_TYPE_COPY_PROGRESS (QEvent::Type)(2006)
class CopyEvent:public QEvent
{
	friend class RequestHandle;
	friend class CopyThread;
	friend class WatchThread;
protected:
	CopyEvent(uint32 curr_index_,QEvent::Type type_):QEvent(type_),curr_index(curr_index_){}
	uint32 curr_index;
};
class CopyErrorEvent:public CopyEvent
{
	friend class RequestHandle;
	friend class CopyThread;
	friend class WatchThread;
private:
	CopyErrorEvent(const ErrorReport& e_r,QEvent::Type type_):CopyEvent(0,type_),error_report(e_r.type(),e_r.code()){}
	ErrorReport error_report;
};
class CopyProgressEvent:public CopyEvent
{
	friend class RequestHandle;
	friend class CopyThread;
	friend class WatchThread;
private:
	CopyProgressEvent(const ProgressReport& p_r,QEvent::Type type_):CopyEvent(0,type_),prog_report(p_r){}
	const ProgressReport prog_report;
};
#endif // EVENTS_H