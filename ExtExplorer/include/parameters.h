#ifndef PARAMETERS_H
#define PARAMETERS_H
#include "type.h"
#include <QString>
class FsIndex
{
public:
	FsIndex(uint32 d_i,uint32 f_i):disk_index(d_i),fs_index(f_i){}
	uint32 diskIndex() const{return disk_index;}
	uint32 fsIndex() const{return fs_index;}
private:
	uint32 disk_index;
	uint32 fs_index;
};
class ParaForScanDirByInodeNum
{
public:
	ParaForScanDirByInodeNum(uint32 d_i,uint32 f_i,uint32 i_n):fs_index(d_i,f_i),inode_num(i_n){}
	uint32 diskIndex() const{return fs_index.diskIndex();}
	uint32 fsIndex() const{return fs_index.fsIndex();}
	uint32 inodeNum() const{return inode_num;}
private:
	FsIndex fs_index;
	uint32 inode_num;
};
class ParaForScanDirByPath
{
public:
	ParaForScanDirByPath(uint32 d_i,uint32 f_i,const QString& path_):fs_index(d_i,f_i),file_path(path_){}
	uint32 diskIndex() const{return fs_index.diskIndex();}
	uint32 fsIndex() const{return fs_index.fsIndex();}
	const QString& path() const{return file_path;}
private:
	FsIndex fs_index;
	QString file_path;
};
class ParaForCopyFile
{
public:
	ParaForCopyFile(const FsIndex& f_i,uint32 i_n,const QString& f_p,const QString& t_p,uint32 f_t):
	  fs_index(f_i),inode_num(i_n),from_path(f_p),to_path(t_p),file_type(f_t){}
	  uint32 diskIndex() const{return fs_index.diskIndex();}
	  uint32 fsIndex() const{return fs_index.fsIndex();}
	  uint32 inodeNum() const{return inode_num;}
	  uint32 fileType() const{return file_type;}
	  const QString& toPath() const{return to_path;}
	  const QString& fromPath() const{return from_path;}
private:
	FsIndex fs_index;
	uint32 inode_num;
	QString from_path;
	QString to_path;
	uint32 file_type;
};

#endif // PARAMETERS_H