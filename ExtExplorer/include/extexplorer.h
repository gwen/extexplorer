#ifndef EXTEXPLORER_H
#define EXTEXPLORER_H
#include <QtGui/QWidget>
#include "ui_extexplorer.h"
#include "type.h"
#include "filelist.h"
#include "requesthandle.h"
#include "filetree.h"
#include "errorhandle.h"
#include "progress.h"
class Node;
class QCloseEvent;
class ErrorReport;
class ExtExplorer : public QWidget,public Ui_ExtExplorerClass
{
	Q_OBJECT

public:
	ExtExplorer(QWidget *parent = 0, Qt::WFlags flags = 0);
	void init();
	~ExtExplorer();
signals:
	void error(const ErrorReport& error_report,uint32 sender);
private slots:
	void on_treeWidget_file_list_itemDoubleClicked(QTreeWidgetItem * item, int column);
	void on_treeWidget_file_tree_itemExpanded(QTreeWidgetItem * item){file_tree.expandNode(static_cast<Node*>(item));}
	void on_treeWidget_file_tree_itemCollapsed(QTreeWidgetItem * item){file_tree.collapseNode(static_cast<Node*>(item));}
	void on_treeWidget_file_tree_itemClicked(QTreeWidgetItem * item,int column){file_tree.listAllFileToFileList(static_cast<Node*>(item));}
	void on_pushButton_up_clicked(){file_list.goUp();}
	void on_pushButton_back_clicked(){file_list.goBack();}
	void on_pushButton_forward_clicked(){file_list.goForward();}
	void on_pushButton_cancel_clicked(){req_handler.cancelCopy();}
	void receiveErrorResult(const ErrorHandleResult& re);
private:
	void closeEvent(QCloseEvent* event_);
	FileList file_list;
	FileTree file_tree;
	RequestHandle req_handler;
	ErrorHandle error_hander;
	Progress progress;
};

#endif // EXTEXPLORER_H
