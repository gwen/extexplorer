#ifndef TRACKCHAIN_H
#define TRACKCHAIN_H
#include <QString>
#include <QList>
#include "node.h"
#include "type.h"
class Node;
class DataForTrack;
class TrackChain 
{
	friend class FileList;
private:
	TrackChain():curr_index(0),tail_index(0),is_checked_in(false){}
	const Node& back(Node& empty_node);
	const Node& forward(Node& empty_node);
	void checkIn(const Node& dir);
	bool isCheckedIn();
	QList<DataForTrack> chain;
	uint32 curr_index; 
	uint32 tail_index;
	bool is_checked_in;
};
class DataForTrack 
{
	friend class TrackChain;
private:
	DataForTrack(const QString& path,uint32 inode_num_):spath(path),inode_num(inode_num_){}
	const QString& path(){return spath;}
	uint32 inodeNum(){return inode_num;}
	QString spath; 
	uint32 inode_num;
};
#endif // TRACKCHAIN_H