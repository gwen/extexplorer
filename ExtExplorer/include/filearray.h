#ifndef FILEARRAY_H
#define FILEARRAY_H
#include "type.h"
#include "extfsdll.h"
#include <QDebug>
template <typename T> class PtrArray;
class File;
class FileArray
{
public:
	FileArray(){}
	FileArray(PtrArray<File>* _files):files(_files){}
	~FileArray(){ExtFsDLL::releaseFilePtrArray(files);}
	File* at(uint32 index) const{return ExtFsDLL::getFilePtr(files,index);}
	uint32 fileCount() const{return ExtFsDLL::getFileCount(files);}
	bool isEmpty(){return files==NULL;}
private:
	PtrArray<File>* files;
};

#endif // FILEARRAY_H