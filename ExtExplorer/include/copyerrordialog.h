#ifndef COPYERRORDIALOG_H
#define COPYERRORDIALOG_H
#include <QtGui/QDialog>
#include "type.h"
#include "ui_copyerror.h"
class QString;
class CopyErrorDialog:public QDialog,public Ui_CopyErrorDialog
{
	Q_OBJECT
public:
	CopyErrorDialog(QWidget* parent);
	void setLabel(const QString& error_type_,const QString& error_code_);
	void setCheckBox(bool is_dir){checkBox_dir->setEnabled(is_dir);}
private:
	static Qt::CheckState default_idf;
	static Qt::CheckState default_idd;
	void setDefaultCheck();
private slots:
	void checkIDF(int state){checkBox_file->setCheckState((Qt::CheckState)state);}
	void setDefaultValue();
	void showOption();
};

#endif // COPYERRORDIALOG_H