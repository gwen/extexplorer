#ifndef NODEARRAY_H
#define NODEARRAY_H
#include <QString>
#include "type.h"
#include "node.h"
class Node;
class FileArray;
class NodeArray
{
	friend class RequestHandle;
	friend class NodeArrayCache;
public:
	NodeArray():node_array(NULL),node_count(0),is_ref(false){}
	NodeArray(const FileArray& file_array_);
	NodeArray(const NodeArray& y);
	~NodeArray(){delete[] node_array;}
	const Node& operator[](uint32 index) const{return node_array[index];}
	Node* at(uint32 index) const{return node_array+index;}
	uint32 nodeCount() const{return node_count;}
	const Node& currentNode() const{return node_array[0];}
	const Node& parentNode() const{return node_array[1];}
	void cancelRef(){is_ref=false;}
	void setRef(){is_ref=true;}
private:
	void addPath(const QString& path);
	bool isRefed() const{return is_ref;}
	Node* node_array;
	uint32 node_count;
	bool is_ref;
};

#endif // NODEARRAY_H