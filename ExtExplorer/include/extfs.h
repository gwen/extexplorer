#ifndef EXTFS_H
#define EXTFS_H
#include "type.h"
template <typename T> class PtrArray;
class File;
class ErrorReport;
class ProgressReport;
class BlockCountReport;
class FileNameReport;
EXTFS_DLL_IMPORT bool dll_openDisks();
EXTFS_DLL_IMPORT void dll_closeDisks();
EXTFS_DLL_IMPORT uint32 dll_diskCount();
EXTFS_DLL_IMPORT uint32 dll_partitionCount(uint32 disk_index);

EXTFS_DLL_IMPORT void dll_initState();
EXTFS_DLL_IMPORT void dll_releaseState();

EXTFS_DLL_IMPORT void dll_getErrorReport(ErrorReport& report);
EXTFS_DLL_IMPORT void dll_suspendCopy();
EXTFS_DLL_IMPORT void dll_cancelCopy(bool is_cancel_all,bool is_del_file);
EXTFS_DLL_IMPORT void dll_continueCopy();

EXTFS_DLL_IMPORT void dll_startWatching();
EXTFS_DLL_IMPORT void dll_watching();
EXTFS_DLL_IMPORT void dll_endWatching();
EXTFS_DLL_IMPORT void dll_stopWatching();
EXTFS_DLL_IMPORT void dll_retry(uint32 is_retry);
EXTFS_DLL_IMPORT void dll_overwrite(bool is_over_write_);

EXTFS_DLL_IMPORT uint32 dll_getReportType();
EXTFS_DLL_IMPORT void dll_getProgressReport(ProgressReport& report);

EXTFS_DLL_IMPORT void dll_releaseFilePtrArray(PtrArray<File>* files);
EXTFS_DLL_IMPORT const uint16* dll_getFileName(File* file);
EXTFS_DLL_IMPORT uint32 dll_getFileNameLength(File* file);
EXTFS_DLL_IMPORT uint64 dll_getFileSize(File* file);
EXTFS_DLL_IMPORT uint32 dll_getFileType(File* file);
EXTFS_DLL_IMPORT uint32 dll_getFileInodeNum(File* file);
EXTFS_DLL_IMPORT File* dll_getFilePtr(PtrArray<File>* files,uint32 index);
EXTFS_DLL_IMPORT uint32 dll_getFileCount(PtrArray<File>* files);

EXTFS_DLL_IMPORT PtrArray<File>* dll_scanDirByInodeNum(uint32 disk_index,uint32 fs_index,uint32 inode_num);
EXTFS_DLL_IMPORT PtrArray<File>* dll_scanDirByPath(uint32 disk_index,uint32 fs_index,const uint16* path);
EXTFS_DLL_IMPORT bool dll_isHaveSubDir(uint32 disk_index,uint32 fs_index,uint32 inode_num);

EXTFS_DLL_IMPORT bool dll_copy(uint32 disk_index,uint32 fs_index,uint32 inode_num,const uint16* to_path);

EXTFS_DLL_IMPORT uint16* dll_fsName(uint32 disk_index,uint32 fs_index);
EXTFS_DLL_IMPORT uint32 dll_fsNameLength(uint32 disk_index,uint32 fs_index);
#endif //EXTFS