#ifndef FILELIST_H
#define FILELIST_H
#include <QObject>
#include <QList>
#include <QMenu>
#include "type.h"
#include "trackchain.h"
class QTreeWidget;
class QComboBox;
class Node;
class NodeArray;
class QTreeWidgetItem;
class FsIndex;
class FileList:public QObject
{
	Q_OBJECT
public:
	FileList():tree_widget(NULL),curr_array(NULL),a_copy(NULL){}
	void setWidgets(QTreeWidget* tree_);
	void goUp();
	void goBack();
	void goForward();
	void openDir(Node& dir);
private slots:
	void listAllFile(const Node& dir,const NodeArray* files,uint32 file_list_flag);
	void copy() const{emit showFileDialog();}
	void copy(const QString& to_path) const;
signals:
	void iNeedNodeArray(const Node& dir,uint32 file_list_flag) const;
	void iNeedNodeArray(const QString& path,uint32 file_list_flag) const;
	void iNeedToCopyFile(QList<QTreeWidgetItem*>& files,const FsIndex& fs_index,const QString& from_path,const QString& to_path) const;
	void showPathInComboBox(const QString& path);
	void showFileDialog() const;
private:
	void clear(); 
	bool eventFilter(QObject* obj,QEvent* event_);
	const NodeArray* curr_array;
	QTreeWidget* tree_widget;
	QMenu pop_menu;
	QAction a_copy;
	TrackChain track_chain;
};
#endif // filelist_H