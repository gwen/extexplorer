#ifndef NODESCACHE_H
#define NODESCACHE_H
#include <QString>
#include <QList>
#include "type.h"
#include "nodearray.h"
#include "filearray.h"
#include "node.h"
class NodeArrayCacheUnit
{
	friend class NodeArrayCache;
	friend class QList<NodeArrayCacheUnit>;
private:
	NodeArrayCacheUnit():node_array(NULL){}
	NodeArrayCacheUnit(const NodeArray* node_array_):node_array(node_array_){}
	void cacheThis(const NodeArray* node_array_)
	{
		delete node_array;
		node_array=node_array_;
	}
	const QString& path() const
	{
		return node_array->currentNode().path();
	}
	bool isNull() const{return node_array==NULL;}
	~NodeArrayCacheUnit(){delete node_array;}
	const NodeArray* node_array;
};
class NodeArrayCache
{
	friend class RequestHandle;
private:
	NodeArrayCache();
	NodeArrayCache(int32 cache_size_);
	~NodeArrayCache();
	const NodeArray* findNodeArray(const QString& path,uint32 flag);
	void cacheNewNodeArray(const NodeArray* node_array,uint32 flag);
	const NodeArray* getNodeArrayFromAnotherCache(int32 node_array_index,uint32 flag);
	void incCurrIndexOfFileList()
	{
		if(++curr_index_of_file_list==cache_size_of_file_list)
			curr_index_of_file_list=0;
	}
	int32 searchInFileListCache(const QString& path) const;
	int32 searchInFileTreeCache(const QString& path) const;
	void cacheFileListNodeArray(const NodeArray* node_array);
	void cacheFileTreeNodeArray(const NodeArray* node_array);

	NodeArrayCacheUnit* cache_of_file_list;
	QList<NodeArrayCacheUnit*> cache_of_file_tree;
	int32 cache_size_of_file_list;
	int32 curr_index_of_file_list;
};
#endif // NODESCACHE_H