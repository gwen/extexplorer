#ifndef MACRO_ICON_PATH_H
#define MACRO_ICON_PATH_H

#define COPY_MENU_ICON "icon/copy.png"
#define UNKNOWN_DISK_ICON "icon/unknown.png"
#define EXT_FS_ICON "icon/linux.png"
#define WIN_FS_ICON "icon/disk.png"
#endif // MACRO_ICON_PATH_H