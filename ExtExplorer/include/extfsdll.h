#ifndef EXTFSDLL_H
#define EXTFSDLL_H
#include "type.h"
#include "extfs.h"
#include "node.h"
#include <QString>
#include <QPixmap>
#include <QIcon>
#include "macro_report.h"
#include "report.h"
#include "parameters.h"
template <typename T> class PtrArray;
class File;

class ExtFsDLL
{
public:
	static bool openDisks(){return dll_openDisks();}
	static void closeDisks(){dll_closeDisks();}
	static uint32 diskCount(){return dll_diskCount();}
	static uint32 partitionCount(uint32 disk_index){return dll_partitionCount(disk_index);}

	static void releaseFilePtrArray(PtrArray<File>* files){dll_releaseFilePtrArray(files);}
	static const uint16* getFileName(File* file){return dll_getFileName(file);}
	static uint32 getFileNameLength(File* file){return dll_getFileNameLength(file);}
	static uint64 getFileSize(File* file){return dll_getFileSize(file);}
	static uint32 getFileType(File* file){return dll_getFileType(file);}
	static uint32 getFileInodeNum(File* file){return dll_getFileInodeNum(file);}
	static File* getFilePtr(PtrArray<File>* files,uint32 index){return dll_getFilePtr(files,index);}
	static uint32 getFileCount(PtrArray<File>* files){return dll_getFileCount(files);}

	static void initState(){dll_initState();}
	static void releaseState(){dll_releaseState();}

	static void suspendCopy(){dll_suspendCopy();}
	static void cancelCopy(bool is_cancel_all,bool is_del_file){dll_cancelCopy(is_cancel_all,is_del_file);}
	static void continueCopy(){dll_continueCopy();}

	static void startWatching(){dll_startWatching();}
	static void watching(){dll_watching();}
	static void endWatching(){return dll_endWatching();}
	static void stopWatching(){dll_stopWatching();}
	static void retry(uint32 is_retry){dll_retry(is_retry);}
	static void overwrite(bool is_over_write_){dll_overwrite(is_over_write_);}

	static uint32 getReportType(){return dll_getReportType();}
	static void getProgressReport(ProgressReport& report)
	{
		dll_getProgressReport(report);
		if(report.progressType()==PROG_FILE_NAME)
			report.setFileName(QString::fromUtf16(report.fileName(),report.fileNameLen()));
	}
	static void getErrorReport(ErrorReport& error_report){dll_getErrorReport(error_report);}

	static PtrArray<File>* scanDir(const ParaForScanDirByInodeNum& para)
	{
		return dll_scanDirByInodeNum(para.diskIndex(),para.fsIndex(),para.inodeNum());
	}
	static PtrArray<File>* scanDir(const ParaForScanDirByPath& para)
	{
		return dll_scanDirByPath(para.diskIndex(),para.fsIndex(),para.path().utf16());
	}
	static bool isHaveSubDir(uint32 disk_index,uint32 fs_index,uint32 inode_num)
	{
		return dll_isHaveSubDir(disk_index,fs_index,inode_num);
	}

	static bool copy(const ParaForCopyFile& para)
	{
		return dll_copy(para.diskIndex(),para.fsIndex(),para.inodeNum(),para.toPath().utf16());
	}

	static uint16* fsName(uint32 disk_index,uint32 fs_index){return dll_fsName(disk_index,fs_index);}
	static uint32 fsNameLength(uint32 disk_index,uint32 fs_index){return dll_fsNameLength(disk_index,fs_index);}

	static QIcon getFileIcon(const QString& file_type);
	static QIcon getDirIcon();
};

#endif // EXTFSDLL_H