#ifndef MACOR_STR_H
#define MACOR_STR_H


#define A_SLASH 0x5C 
#define SLASH 0x2F 
#define STR_SLASH "/"
#define CH_SLASH '/'
#define STR_ASLASH "\\"
#define CH_ASLASH '\\'
#define DOT '.'

#endif // MACOR_STR_H