#ifndef NODE_H
#define NODE_H
#include <QTreeWidgetItem>
#include <QString>
#include <QList>
#include <QIcon>
#include "type.h"
class File;
class NodeArray;
class Icon
{
	friend class Node;
private:
	Icon(const QString& name_,const QIcon& icon_):name(name_),icon(icon_){}
	QString name;
	QIcon icon;
};
class Node:public QTreeWidgetItem
{
	friend class NodeArray;
	friend class RequestHandle;
public:
	Node():
	  QTreeWidgetItem(0),file_ptr(NULL),spath(),my_node_array(NULL),file_type(0),file_name(),inode_num(0){}
	Node(const QString& path_,uint32 inode_num_):inode_num(inode_num_),spath(path_),file_name(),file_type(0),my_node_array(NULL){}
	const Node& setPathAndInodeNum(const QString& path_,uint32 inode_num_)
	{
		spath=path_;
		inode_num=inode_num_;
		return *this;
	}
	Node& operator=(const Node& y);
	const Node& copyTo(Node& y) const;
	const File* file() const{return file_ptr;}
	const QString& path() const{return spath;}
	void setPath(const QString& parent_path);
	const QString& name() const{return file_name;}
	uint32 type() const{return file_type;}
	uint32 inodeNum() const{return inode_num;}
	bool isNodeArrayNull() const{return my_node_array==NULL?true:false;}
	bool isPathEmpty(){return spath.isEmpty();}
	void cancelNodeArrayRef();
	void setNodeArrayRef();
	void setNodeArray(NodeArray* node_array){my_node_array=node_array;}
	void setRootNodeData(const QString& text_str,const QString& path_str);
	void setDiskNodeData(const QString& text_str,const QString& path_str);
	~Node();
private:
	void setNodeData(File* _file);
	void setNodeIcon();
	const QString fileStrSize(uint64 file_size) const;
	File* file_ptr;
	QString spath;
	QString file_name;
	NodeArray* my_node_array;
	uint32 inode_num;
	uint32 file_type;
	static QList<Icon> icons;
	static QIcon dir_icon;
	static QIcon default_icon;
	static QIcon disk_icon;
};
#endif // NODE_H