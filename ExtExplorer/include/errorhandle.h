#ifndef ERRORHANDLE_H
#define ERRORHANDLE_H
#include "type.h"
#include <QObject>
#include <QtGui/QWidget>
#include <QFileDialog>
#include <QMessageBox>
class ErrorReport;
class ErrorHandleResult
{
	friend class ErrorHandle;
public:
	uint32 receiver() const{return re;}
	bool yesOrNo() const{return yes_or_no;}
	const ErrorReport& errorReport() const{return error_report;}
protected:
	ErrorHandleResult(uint32 receiver_,const ErrorReport& er,bool yon):re(receiver_),error_report(er),yes_or_no(yon){}
	uint32 re;
	const ErrorReport& error_report;
	bool yes_or_no;
};
class CopyErrorResult:public ErrorHandleResult
{
	friend class ErrorHandle;
public:
	bool yesOrNo() const{return yes_or_no;}
	bool isDeleteDir() const{return is_del_dir;}
	bool isDeleteFile() const{return is_del_file;}
private:
	CopyErrorResult(uint32 receiver_,const ErrorReport& er,bool yon,bool idf,bool idd):
	ErrorHandleResult(receiver_,er,yon),is_del_file(idf),is_del_dir(idd){}
	bool is_del_file;
	bool is_del_dir;
};
class CancelCopyResult:public ErrorHandleResult
{
	friend class ErrorHandle;
public:
	bool yesOrNo() const{return yes_or_no;}
	bool isCancelOther() const{return is_cancel_other;}
	bool isCancelAll() const{return is_cancel_all;}
	bool isDeleteDir() const{return is_del_dir;}
	bool isDeleteAll() const{return is_del_all;}
	bool isDeleteFile() const{return is_del_file;}
private:
	CancelCopyResult(uint32 receiver_,const ErrorReport& er,bool yon,bool ico,bool ica,bool ida,bool idf,bool idd):
	   ErrorHandleResult(receiver_,er,yon),is_cancel_other(ico),is_cancel_all(ica),
		   is_del_all(ida),is_del_file(idf),is_del_dir(idd){}
	   bool is_cancel_other;
	   bool is_cancel_all;
	   bool is_del_all;
	   bool is_del_file;
	   bool is_del_dir;
};
class ErrorHandle : public QObject
{
	Q_OBJECT
public:
	ErrorHandle(QWidget* dialog_parent_);
signals:
	void handleResult(const ErrorHandleResult& re);
	void dirPath(const QString& path);
private slots:
	void receiveError(const ErrorReport& error_report,uint32 sender);
	void showFileDialog(){file_dialog.exec();}
	void selectedDir(const QString& path){emit dirPath(path);}
private:
	void initErrorDialog();
	void handleCanleCopy(const ErrorReport& er,uint32 sender);
	void handleWriteError(const ErrorReport& er,uint32 sender);
	void handleCreateError(const ErrorReport& er,uint32 sender);
	void handleReadError(const ErrorReport& er,uint32 sender);
	void handleFileNotFound();
	void handleCloseApp(const ErrorReport& er,uint32 sender);
	void showErrorDialog(const QString& type_,const QString& code_);
	QWidget* dialog_parent;
	QFileDialog file_dialog;
	QMessageBox error_dialog;
	bool is_show_cdialog;
	bool is_show_edialog;
};

#endif // ERRORHANDLE_H