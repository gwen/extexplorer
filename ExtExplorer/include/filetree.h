#ifndef FILETREE_H
#define FILETREE_H
#include <QObject>
#include "type.h"
#define CHECK_DIR_EMPTY
class QTreeWidget;
class QTreeWidgetItem;
class QComboBox;
class QPushButton;
class Node;
class NodeArray;
class FileTree:public QObject
{
	Q_OBJECT
public:
	FileTree():tree_widget(NULL),disk_nodes(NULL),partition_nodes(NULL){}
	~FileTree();
	void setWidgets(QTreeWidget* tree,QComboBox* path,QComboBox* fs,QPushButton* go);
	void init();
	void expandNode(Node* dir);
	void collapseNode(Node* dir);
	void listAllFileToFileList(Node* dir) const;
	void listAllFileToFileList(const QString& path) const;
	void collapseAllNode();
signals:
	void iNeedNodeArray(const Node& dir,uint32 file_tree_flag) const;
	void iNeedNodeArray(const QString& path,uint32 file_list_flag) const;
private slots:
	void listSubDir(const Node& dir,const NodeArray* node_array,uint32 file_tree_flag);
	void showPathInComboBox(const QString& path);
private:
	void expandDiskNode(Node* disk);
	void removeNodeTree(Node* dir);
	void addDefaultNodeFor(Node* dir);
	void removeDefaultNodeFor(Node* dir);
	void setFsName(uint32 disk_index,uint32 fs_index);
	bool eventFilter(QObject* obj,QEvent* event_);
	const QString& findFsIndex(const QString& fs_name) const;
	QTreeWidget* tree_widget;
	uint32 disk_count;
	Node* disk_nodes;
	Node** partition_nodes;
	QComboBox* combo_box_file_path;
	QComboBox* combo_box_fs;
	QPushButton* pushButton_go;
};

#endif // FILETREE_H