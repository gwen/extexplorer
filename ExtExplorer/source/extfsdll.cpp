#include "../include/extfsdll.h"
#include <windows.h>
QIcon ExtFsDLL::getFileIcon(const QString& file_type)
{
	SHFILEINFO info;
	SHGetFileInfo((LPCTSTR)file_type.utf16(),
		FILE_ATTRIBUTE_NORMAL,
		&info,
		sizeof(SHFILEINFO),
		SHGFI_ICON|SHGFI_LARGEICON|SHGFI_USEFILEATTRIBUTES);
	QIcon tmp_icon=QIcon(QPixmap::fromWinHICON(info.hIcon));
	CloseHandle(info.hIcon);
	return tmp_icon;
}
QIcon ExtFsDLL::getDirIcon()
{
	SHFILEINFO info;
	QString dir("dir");
	SHGetFileInfo((LPCTSTR)dir.utf16(),
		FILE_ATTRIBUTE_DIRECTORY,
		&info,
		sizeof(SHFILEINFO),
		SHGFI_ICON|SHGFI_LARGEICON|SHGFI_USEFILEATTRIBUTES);
	QIcon tmp_icon=QIcon(QPixmap::fromWinHICON(info.hIcon));
	CloseHandle(info.hIcon);
	return tmp_icon;
}
