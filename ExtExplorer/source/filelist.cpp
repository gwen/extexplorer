#include "../include/filelist.h"
#include "../include/node.h"
#include "../include/nodearray.h"
#include "../include/macro_file_type.h"
#include "../include/macro_str.h"
#include "../include/macro_signal.h"
#include "../include/macro_constant.h"
#include "../include/macro_gui_str.h"
#include "../include/macro_icon_path.h"
#include "../include/parameters.h"
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QHeaderView>
#include <QContextMenuEvent>
#include <QIcon>
#include <QChar>
#include <QDebug>
void FileList::listAllFile(const Node& dir,const NodeArray* files,uint32 file_list_flag)
{
	if(file_list_flag!=FILELIST_NODEARRAY)
		return;
	clear();
	curr_array=files;
	emit showPathInComboBox(curr_array->currentNode().path());
	for (uint32 i=FILE_START_INDEX;i<files->nodeCount();i++)
		tree_widget->addTopLevelItem(files->at(i));
	if(!track_chain.isCheckedIn())
		track_chain.checkIn(dir);
}
void FileList::openDir(Node& dir)
{
	if(dir.type()!=DIR_ENTRY_FILE_TYPE_DIR)
		return;
	if(dir.path().isEmpty())
		dir.setPath(curr_array->currentNode().path());
	emit iNeedNodeArray(dir.copyTo(Node()),FILELIST_NODEARRAY);
}
void FileList::clear()
{
	tree_widget->setCurrentItem(NULL);
	uint32 item_count=tree_widget->topLevelItemCount();
	for (uint32 i=0;i<item_count;i++)
		tree_widget->takeTopLevelItem(0);
}
void FileList::goUp()
{
	if(curr_array->currentNode().path().count(CH_SLASH)>3)
		emit iNeedNodeArray(curr_array->parentNode().copyTo(Node()),FILELIST_NODEARRAY);
}
void FileList::goBack()
{
	Node tmp_node;
	if(!track_chain.back(tmp_node).path().isEmpty())
	{
		if(tmp_node.inodeNum()!=0)
			emit iNeedNodeArray(tmp_node,FILELIST_NODEARRAY);
		else
			emit iNeedNodeArray(tmp_node.path(),FILELIST_NODEARRAY);
	}

}
void FileList::goForward()
{
	Node tmp_node;
	if(!track_chain.forward(tmp_node).path().isEmpty())
	{
		if(tmp_node.inodeNum()!=0)
			emit iNeedNodeArray(tmp_node,FILELIST_NODEARRAY);
		else
			emit iNeedNodeArray(tmp_node.path(),FILELIST_NODEARRAY);
	}
}
void FileList::setWidgets(QTreeWidget* tree_)
{
	tree_widget=tree_;
	QStringList header_label;
	header_label<<FILE_NAME_STR<<FILE_SIZE_STR;
	tree_widget->setHeaderLabels(header_label);
	tree_widget->header()->resizeSection(0,FILE_NAME_HEADER_WIDTH);
	tree_widget->header()->resizeSection(1,FILE_SIZE_HEADER_WIDTH);
	tree_widget->installEventFilter(this);
	a_copy.setText(COPY_STR);
	a_copy.setIcon(QIcon(COPY_MENU_ICON));
	a_copy.setParent(tree_widget->parent());
	connect(&a_copy,SIGNAL(triggered()),this,SLOT(copy()));
	pop_menu.addAction(&a_copy);
}
bool FileList::eventFilter(QObject* obj, QEvent* event_)
{
	if(obj==tree_widget&&event_->type()==QEvent::ContextMenu)
	{
		if(tree_widget->selectedItems().size()!=0)
			pop_menu.exec(static_cast<QContextMenuEvent*>(event_)->globalPos());
		return true;
	}
	return false;
}
void FileList::copy(const QString& to_path) const
{
	QList<QTreeWidgetItem*> files=tree_widget->selectedItems();
	uint32 disk_index=curr_array->currentNode().path().section(CH_SLASH,1,1).toInt();
	uint32 fs_index=curr_array->currentNode().path().section(CH_SLASH,2,2).toInt();
	FsIndex index(disk_index,fs_index);
	QString from_path=CH_SLASH+curr_array->currentNode().path().section(CH_SLASH,3,-1);
	const_cast<QString&>(to_path).replace(QString(STR_SLASH),QString(STR_ASLASH));
	if(to_path[to_path.size()-1]!=QChar(CH_ASLASH))
		const_cast<QString&>(to_path)+=STR_ASLASH;
	emit iNeedToCopyFile(files,index,from_path,to_path);
}