#include "../include/copyerrordialog.h"
#include <QString>
CopyErrorDialog::CopyErrorDialog(QWidget* parent):QDialog(parent)
{
	setupUi(this);
	this->setFixedSize(365,140);
	frame_option->setVisible(false);
	connect(checkBox_dir,SIGNAL(stateChanged(int)),this,SLOT(checkIDF(int)));
	connect(okButton,SIGNAL(clicked()),this,SLOT(setDefaultValue()));
	connect(pushButton_option,SIGNAL(clicked()),this,SLOT(showOption()));
	setDefaultCheck();
}
void CopyErrorDialog::setDefaultCheck()
{
	checkBox_dir->setCheckState(default_idd);
	checkBox_file->setCheckState(default_idf);
}
void CopyErrorDialog::setDefaultValue()
{
	if(checkBox_is_show->isChecked())
	{
		CopyErrorDialog::default_idd=checkBox_dir->checkState();
		CopyErrorDialog::default_idf=checkBox_file->checkState();
	}
}
void CopyErrorDialog::setLabel(const QString& error_type_,const QString& error_code_)
{
	label_error_type->setText(error_type_);
	label_error_code->setText(error_code_);
}
void CopyErrorDialog::showOption()
{
	if(!frame_option->isVisible())
	{
		this->setFixedSize(365,200);
		frame_option->setVisible(true);
	}
	else
	{
		this->setFixedSize(365,140);
		frame_option->setVisible(false);
	}
}
Qt::CheckState CopyErrorDialog::default_idd=Qt::Checked;
Qt::CheckState CopyErrorDialog::default_idf=Qt::Checked;