#include "../include/extexplorer.h"
#include <QtGui/QApplication>
#include <QTextCodec>
#include <QString>
#pragma comment(lib,"ExtFs.lib")
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QString sPath = a.applicationDirPath();
	sPath += QString("/plugins");
	a.addLibraryPath(sPath);
	ExtExplorer w;
	w.show();
	return a.exec();
}
