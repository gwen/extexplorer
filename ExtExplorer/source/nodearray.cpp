#include "../include/nodearray.h"
#include "../include/filearray.h"

#include "../include/macro_str.h"
NodeArray::NodeArray(const FileArray& file_array_)
{
	node_count=file_array_.fileCount();
	is_ref=false;
	node_array=new Node[node_count];
	for (uint32 i=0;i<node_count;i++)
		node_array[i].setNodeData(file_array_.at(i));
}
void NodeArray::addPath(const QString& path)
{
	node_array[0].spath=path;
	node_array[1].spath=path.section(CH_SLASH,0,-3);
	node_array[1].spath+=CH_SLASH;
}
NodeArray::NodeArray(const NodeArray& y)
{
	node_count=y.node_count;
	is_ref=false;
	node_array=new Node[node_count];
	for(uint32 i=0;i<node_count;i++)
		node_array[i]=y.node_array[i];
}
