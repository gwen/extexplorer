#include "../include/cancelcopydialog.h"
CancelCopyDialog::CancelCopyDialog(QWidget* parent):QDialog(parent)
{
	setupUi(this);
	frame_option->setVisible(false);
	this->setFixedSize(450,130);
	connect(checkBox_del_dir,SIGNAL(stateChanged(int)),this,SLOT(checkIDD(int)));
	connect(checkBox_cancel_all,SIGNAL(stateChanged(int)),this,SLOT(checkICA(int)));
	connect(checkBox_del_all,SIGNAL(stateChanged(int)),this,SLOT(checkIDA(int)));
	connect(okButton,SIGNAL(clicked()),this,SLOT(setDefaultValue()));
	connect(pushButton_option,SIGNAL(clicked()),this,SLOT(showOption()));
	setDefaultCheck();
}
void CancelCopyDialog::setCheckBox(bool ico)
{
	is_dir_enable=ico;
}
void CancelCopyDialog::checkICA(int state)
{
	checkBox_cancel_other->setCheckState((Qt::CheckState)state);
	checkBox_del_all->setCheckState((Qt::CheckState)state);
	bool is_enable=((Qt::CheckState)state==Qt::Checked?false:true);
	checkBox_del_all->setEnabled(!is_enable);
	if(!is_enable||(!checkBox_del_dir->isChecked()&&is_dir_enable))
		checkBox_cancel_other->setEnabled(is_enable);
}
void CancelCopyDialog::checkIDD(int state)
{
	bool is_enable=((Qt::CheckState)state==Qt::Checked?false:true);
	checkBox_cancel_other->setEnabled(is_enable);
	checkBox_cancel_other->setCheckState((Qt::Checked));
	checkBox_del_incomp->setEnabled(is_enable);
	checkBox_del_incomp->setCheckState((Qt::Checked));
}
void CancelCopyDialog::checkIDA(int state)
{
	bool is_enable=((Qt::CheckState)state==Qt::Checked?false:true);
	if(is_dir_enable)
		checkBox_del_dir->setEnabled(is_enable);
	checkBox_del_dir->setCheckState(Qt::Checked);
	checkBox_cancel_all->setEnabled(is_enable);
	if(!is_enable||!checkBox_del_dir->isChecked()||!is_dir_enable)
		checkBox_del_incomp->setEnabled(is_enable);
}
void CancelCopyDialog::setDefaultCheck()
{
	checkBox_del_all->setCheckState(CancelCopyDialog::default_ida);
	checkBox_del_dir->setCheckState(CancelCopyDialog::default_idd);
	checkBox_del_incomp->setCheckState(CancelCopyDialog::default_idf);
	checkBox_cancel_all->setCheckState(CancelCopyDialog::default_ica);
	checkBox_cancel_other->setCheckState(CancelCopyDialog::default_ico);
	checkBox_del_dir->setEnabled(false);
	checkBox_del_incomp->setEnabled(false);
	checkBox_cancel_other->setEnabled(false);
	checkBox_cancel_all->setEnabled(false);
	checkBox_is_show->setCheckState(Qt::Unchecked);
}
void CancelCopyDialog::setDefaultValue()
{
	if(checkBox_is_show->isChecked())
	{
		CancelCopyDialog::default_ica=checkBox_cancel_all->checkState();
		CancelCopyDialog::default_ico=checkBox_cancel_other->checkState();
		CancelCopyDialog::default_ida=checkBox_del_all->checkState();
		CancelCopyDialog::default_idd=checkBox_del_dir->checkState();
		CancelCopyDialog::default_idf=checkBox_del_incomp->checkState();
	}
}
void CancelCopyDialog::showOption()
{
	if(!frame_option->isVisible())
	{
		this->setFixedSize(450,230);
		frame_option->setVisible(true);
	}
	else
	{
		this->setFixedSize(450,130);
		frame_option->setVisible(false);
	}
}
Qt::CheckState CancelCopyDialog::default_ica=Qt::Checked;
Qt::CheckState CancelCopyDialog::default_ico=Qt::Checked;
Qt::CheckState CancelCopyDialog::default_ida=Qt::Checked;
Qt::CheckState CancelCopyDialog::default_idd=Qt::Checked;
Qt::CheckState CancelCopyDialog::default_idf=Qt::Checked;