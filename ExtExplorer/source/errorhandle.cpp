#include "../include/errorhandle.h"
#include "../include/extfsdll.h"
#include "../include/copyerrordialog.h"
#include "../include/cancelcopydialog.h"
#include "../include/macro_file_type.h"
#include "../include/macro_report.h"
#include "../include/macro_gui_str.h"
#include "../include/report.h"
#include <QMessageBox>
#include <windows.h>
#include <QString>
#include <QAbstractButton>
#include <QPixmap>
#include <QTextCodec>
ErrorHandle::ErrorHandle(QWidget* dialog_parent_):dialog_parent(dialog_parent_),file_dialog(dialog_parent_),
	error_dialog(dialog_parent_),is_show_cdialog(true),is_show_edialog(true)
{
	QTextCodec::setCodecForTr(QTextCodec::codecForName("GBK"));
	file_dialog.setFileMode(QFileDialog::Directory);
	file_dialog.setOptions(QFileDialog::ShowDirsOnly|QFileDialog::HideNameFilterDetails);
	file_dialog.setLabelText(QFileDialog::FileName,tr("文件夹:"));
	file_dialog.setLabelText(QFileDialog::FileType,tr("文件类型:"));
	file_dialog.setLabelText(QFileDialog::Accept,tr("确定"));
	file_dialog.setLabelText(QFileDialog::Reject,tr("取消"));
	file_dialog.setLabelText(QFileDialog::LookIn,tr("选择路径:"));
	file_dialog.setViewMode(QFileDialog::List);
	connect(&file_dialog,SIGNAL(fileSelected(const QString&)),this,SLOT(selectedDir(const QString&)));
	initErrorDialog();
}
void ErrorHandle::initErrorDialog()
{
	error_dialog.setIcon(QMessageBox::Critical);
	error_dialog.addButton(CONFIRM_STR,QMessageBox::YesRole);
	error_dialog.setWindowTitle(ERROR_STR);
	error_dialog.setWindowIcon(QIcon(error_dialog.iconPixmap()));
}
void ErrorHandle::receiveError(const ErrorReport& error_report,uint32 sender)
{
	switch(error_report.type())
	{
	case ERROR_TYPE_COPY_CANCEL:
		handleCanleCopy(error_report,sender);
		break;
	case ERROR_TYPE_WRITEFILE:
		handleWriteError(error_report,sender);
		break;
	case ERROR_TYPE_CREATEFILE:
		handleCreateError(error_report,sender);
		break;
	case ERROR_TYPE_FIlE_NOT_FOUND:
		handleFileNotFound();
		break;
	case ERROR_TYPE_CLOSE_APP:
		handleCloseApp(error_report,sender);
		break;
	case ERROR_TYPE_OPENDISK:
		showErrorDialog(OPENDISK_ERROR_STR,QString::number(error_report.type()));
		break;
	default:
		;
	}
}
void ErrorHandle::handleCanleCopy(const ErrorReport& er,uint32 sender)
{
	CancelCopyDialog dialog(dialog_parent);
	if(*(uint32*)er.extraInfo()==DIR_ENTRY_FILE_TYPE_NORMAL)
		dialog.setCheckBox(false);
	else if(*(uint32*)er.extraInfo()==DIR_ENTRY_FILE_TYPE_DIR)
		dialog.setCheckBox(true);
	bool yes_or_no=true;
	if(is_show_cdialog)
	{
		yes_or_no=(dialog.exec()==QDialog::Accepted?true:false);
		if(yes_or_no)
			is_show_cdialog=!dialog.checkBox_is_show->isChecked();

	}
	emit handleResult(CancelCopyResult(sender,er,yes_or_no,
		dialog.checkBox_cancel_other->isChecked(),
		dialog.checkBox_cancel_all->isChecked(),
		dialog.checkBox_del_all->isChecked(),
		dialog.checkBox_del_incomp->isChecked(),
		dialog.checkBox_del_dir->isChecked()));
}
void ErrorHandle::handleWriteError(const ErrorReport& er,uint32 sender)
{
	CopyErrorDialog dialog(dialog_parent);
	QString label_type_str=WRITE_ERROR_STR;
	QString label_code_str=QString::number(er.code());
	if(er.code()==ERROR_DISK_FULL)
	{
		label_code_str+="(";
		label_code_str+=DISK_FULL_STR;
		label_code_str+=")";
	}
	dialog.setLabel(label_type_str,label_code_str);
	if(*(uint32*)er.extraInfo()==DIR_ENTRY_FILE_TYPE_NORMAL)
		dialog.setCheckBox(false);
	else if(*(uint32*)er.extraInfo()==DIR_ENTRY_FILE_TYPE_DIR)
		dialog.setCheckBox(true);
	bool yes_or_no=true;
	if(is_show_edialog)
	{
		yes_or_no=dialog.exec()==QDialog::Accepted;
		if(yes_or_no)
			is_show_edialog=!dialog.checkBox_is_show->isChecked();
	}
	emit handleResult(CopyErrorResult(sender,er,yes_or_no,
		dialog.checkBox_file->isChecked(),
		dialog.checkBox_dir->isChecked()));
}
void ErrorHandle::handleCreateError(const ErrorReport& er,uint32 sender)
{
	bool yes_or_no=false;
	if(er.code()==ERROR_FILE_EXISTS||er.code()==ERROR_ALREADY_EXISTS)
	{
		QMessageBox error_dialog(dialog_parent);
		QAbstractButton* yes_button=error_dialog.addButton(YES_STR,QMessageBox::YesRole);
		QAbstractButton* no_button=error_dialog.addButton(NO_STR,QMessageBox::NoRole);
		error_dialog.setIcon(QMessageBox::Question);
		error_dialog.setWindowTitle(FILE_EXIST_STR1);
		error_dialog.setWindowIcon(QIcon(error_dialog.iconPixmap()));
		error_dialog.setText(FILE_EXIST_STR2);
		error_dialog.exec();
		yes_or_no=error_dialog.clickedButton()==yes_button;
	}
	else
		showErrorDialog(CREATE_ERROR_STR,QString::number(er.code()));
	emit handleResult(ErrorHandleResult(sender,er,yes_or_no));
}
void ErrorHandle::handleReadError(const ErrorReport& er,uint32 sender)
{
	if(er.extraInfo()!=NULL)
	{
		CopyErrorDialog dialog(dialog_parent);
		dialog.setLabel(READ_ERROR_STR,QString::number(er.code()));
		if(*(uint32*)er.extraInfo()==DIR_ENTRY_FILE_TYPE_NORMAL)
			dialog.setCheckBox(false);
		else if(*(uint32*)er.extraInfo()==DIR_ENTRY_FILE_TYPE_DIR)
			dialog.setCheckBox(true);
		bool yes_or_no=true;
		if(is_show_edialog)
		{
			yes_or_no=dialog.exec()==QDialog::Accepted;
			if(yes_or_no)
				is_show_edialog=!dialog.checkBox_is_show->isChecked();
		}
		emit handleResult(CopyErrorResult(sender,er,yes_or_no,
			dialog.checkBox_file->isChecked(),
			dialog.checkBox_dir->isChecked()));
	}
	else
		showErrorDialog(CREATE_ERROR_STR,QString::number(er.code()));
}
void ErrorHandle::handleFileNotFound()
{
	QMessageBox error_dialog(dialog_parent);
	error_dialog.addButton(CONFIRM_STR,QMessageBox::YesRole);
	error_dialog.setIcon(QMessageBox::Warning);
	error_dialog.setWindowTitle(FILE_NOT_FOUND_STR);
	error_dialog.setWindowIcon(QIcon(error_dialog.iconPixmap()));
	error_dialog.setText(FILE_NOT_FOUND_STR);
	error_dialog.exec();
}
void ErrorHandle::showErrorDialog(const QString& type_,const QString& code_)
{
	QString error_type_str=ERROR_TYPE_STR;
	error_type_str+=type_;
	QString error_code_str=ERROR_CODE_STR;
	error_code_str+=code_;
	error_dialog.setText(error_type_str);
	error_dialog.setInformativeText(error_code_str);
	error_dialog.exec();
}
void ErrorHandle::handleCloseApp(const ErrorReport& er,uint32 sender)
{
	QMessageBox error_dialog(dialog_parent);
	QAbstractButton* yes_button=error_dialog.addButton(YES_STR,QMessageBox::YesRole);
	QAbstractButton* no_button=error_dialog.addButton(NO_STR,QMessageBox::NoRole);
	error_dialog.setIcon(QMessageBox::Question);
	error_dialog.setWindowTitle(IS_CLOSE_STR);
	error_dialog.setWindowIcon(QIcon(error_dialog.iconPixmap()));
	error_dialog.setText(COPYING_FILE_STR);
	error_dialog.exec();
	bool yes_or_no=(error_dialog.clickedButton()==yes_button?true:false);
	emit handleResult(ErrorHandleResult(sender,er,yes_or_no));
}