#include "../include/progress.h"
#include <QtGui/QApplication>
#include <QProgressBar>
#include <QLabel>
#include <QFrame>
#include "../include/extfsdll.h"
#include "../include/events.h"
#include "../include/macro_str.h"
#include "../include/macro_file_type.h"
#include "../include/macro_report.h"
void Progress::start(const QString& from_path,const QString& to_path,uint32 file_type)
{
	QString file_name=to_path.section(CH_ASLASH,-1,-1);
	if(file_type==DIR_ENTRY_FILE_TYPE_NORMAL)
	{
		QString real_to_path=to_path.section(CH_ASLASH,0,-2)+STR_ASLASH;
		label_file_name->setText(file_name);
		label_to_path->setText(real_to_path);
		label_from_path->setText(from_path);
	}
	else
	{
		QString real_from_path=from_path+file_name+STR_SLASH;
		label_to_path->setText(to_path);
		label_from_path->setText(real_from_path);
		label_file_name->setText("");
	}
	copy_progress_bar->setValue(0);
}
void Progress::start()
{
	frame_progress->setVisible(true);
}
void Progress::end()
{
	frame_progress->setVisible(false);
}
void Progress::setWidgets(QProgressBar* bar,QLabel* to,QLabel* from,QLabel* name,QFrame* frame)
{
	copy_progress_bar=bar;
	copy_progress_bar->setValue(0);
	label_file_name=name;
	label_from_path=from;
	label_to_path=to;
	frame_progress=frame;
	frame->setVisible(false);
}
void Progress::receiveProgress(const ProgressReport& pr)
{
	switch(pr.progressType())
	{
	case PROG_COPYED_BLOCK_COUNT:
		copy_progress_bar->setValue(pr.progressValue());
		break;
	case PROG_TOTAL_BLOCK_COUNT:
		copy_progress_bar->setMaximum(pr.progressValue());
		break;
	case PROG_FILE_NAME:
		label_file_name->setText(pr.progressFileName());
		break;
	default:
		;
	}
}