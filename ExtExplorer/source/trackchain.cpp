#include "../include/trackchain.h"

void TrackChain::checkIn(const Node& dir)
{
	uint32 inode_num=dir.inodeNum();
	if(curr_index>=chain.size())
		chain.append(DataForTrack(dir.path(),inode_num));
	else
		chain.replace(curr_index,DataForTrack(dir.path(),inode_num));
	curr_index++;
	tail_index=curr_index;
}
const Node& TrackChain::back(Node& empty_node)
{
	if(curr_index>1)
	{
		curr_index--;
		is_checked_in=true;
		return empty_node.setPathAndInodeNum(chain[curr_index-1].path(),chain[curr_index-1].inodeNum());
	}
	return empty_node;
}
const Node& TrackChain::forward(Node& empty_node)
{
	if(curr_index<tail_index)
	{
		curr_index++;
		is_checked_in=true;
		return empty_node.setPathAndInodeNum(chain[curr_index-1].path(),chain[curr_index-1].inodeNum());
	}
	return empty_node;
}
bool TrackChain::isCheckedIn()
{
	bool tmp=is_checked_in;
	if(is_checked_in)
		is_checked_in=false;
	return tmp;
}