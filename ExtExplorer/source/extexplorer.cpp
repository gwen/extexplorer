#include "../include/extexplorer.h"
#include <QTextCodec>
#include <QCloseEvent>
#include <QDebug>
#include "../include/macro_signal.h"
#include "../include/extfsdll.h"
#include "../include/report.h"
#define CACHE_SIZE 32
ExtExplorer::ExtExplorer(QWidget *parent, Qt::WFlags flags)
	: QWidget(parent, flags),file_list(),file_tree(),req_handler(CACHE_SIZE),error_hander(this)
{
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("GBK"));
	init();
}
void ExtExplorer::init()
{
	setupUi(this);
	file_list.setWidgets(treeWidget_file_list);
	file_tree.setWidgets(treeWidget_file_tree,comboBox_file_path,comboBox_fs,pushButton_go);
	progress.setWidgets(progressBar_copy,label_to_path,label_from_path,label_file_name,frame_copy);
	connect(&file_list,SIGNAL(iNeedNodeArray(const Node&,uint32)),
		&req_handler,SLOT(needNodeArray(const Node&,uint32)));
	connect(&file_list,SIGNAL(iNeedNodeArray(const QString&,uint32)),
		&req_handler,SLOT(needNodeArray(const QString&,uint32)));
	connect(&file_tree,SIGNAL(iNeedNodeArray(const Node&,uint32)),
		&req_handler,SLOT(needNodeArray(const Node&,uint32)));
	connect(&file_tree,SIGNAL(iNeedNodeArray(const QString&,uint32)),
		&req_handler,SLOT(needNodeArray(const QString&,uint32)));
	connect(&file_list,SIGNAL(showPathInComboBox(const QString&)),&file_tree,SLOT(showPathInComboBox(const QString&)));

	connect(&req_handler,SIGNAL(sendNodeArray(const Node&,const NodeArray*,uint32)),
		&file_list,SLOT(listAllFile(const Node&,const NodeArray*,uint32)));
	connect(&req_handler,SIGNAL(sendNodeArray(const Node&,const NodeArray*,uint32)),
		&file_tree,SLOT(listSubDir(const Node&,const NodeArray*,uint32)));
	connect(&file_list,SIGNAL(iNeedToCopyFile(QList<QTreeWidgetItem*>&,const FsIndex&,const QString&,const QString&)),
		&req_handler,SLOT(needToCopyFile(QList<QTreeWidgetItem*>&,const FsIndex&,const QString&,const QString& )));

	connect(&req_handler,SIGNAL(copyStart(const QString&,const QString&,uint32)),
		&progress,SLOT(start(const QString&,const QString&,uint32)));
	connect(&req_handler,SIGNAL(copyProgress(const ProgressReport&)),
		&progress,SLOT(receiveProgress(const ProgressReport&)));
	connect(&req_handler,SIGNAL(allCopyStart()),&progress,SLOT(start()));
	connect(&req_handler,SIGNAL(allCopyEnd()),&progress,SLOT(end()));

	connect(&req_handler,SIGNAL(error(const ErrorReport&,uint32)),
		&error_hander,SLOT(receiveError(const ErrorReport&,uint32)));
	connect(&error_hander,SIGNAL(handleResult(const ErrorHandleResult&)),
		&req_handler,SLOT(receiveErrorResult(const ErrorHandleResult&)));
	connect(&file_list,SIGNAL(showFileDialog()),&error_hander,SLOT(showFileDialog()));
	connect(&error_hander,SIGNAL(dirPath(const QString&)),&file_list,SLOT(copy(const QString&)));

	connect(this,SIGNAL(error(const ErrorReport&,uint32)),&error_hander,SLOT(receiveError(const ErrorReport&,uint32)));
	connect(&error_hander,SIGNAL(handleResult(const ErrorHandleResult&)),this,SLOT(receiveErrorResult(const ErrorHandleResult&)));
	ExtFsDLL::initState();
	if(!ExtFsDLL::openDisks())
	{
		ErrorReport error_report;
		ExtFsDLL::getErrorReport(error_report);
		emit error(error_report,EXTEXPLORER);
	}
	file_tree.init();
}
void ExtExplorer::on_treeWidget_file_list_itemDoubleClicked(QTreeWidgetItem* item, int column)
{
		file_list.openDir(*static_cast<Node*>(item));
}
void ExtExplorer::closeEvent(QCloseEvent* event_)
{
	if(req_handler.isCopying())
	{
		emit error(ErrorReport(ERROR_TYPE_CLOSE_APP,0),EXTEXPLORER);
		event_->ignore();
	}
	else
		event_->accept();
}
void ExtExplorer::receiveErrorResult(const ErrorHandleResult& re)
{
	if(re.receiver()!=EXTEXPLORER)
		return;
	if(re.yesOrNo())
		QApplication::exit();
}
ExtExplorer::~ExtExplorer()
{
	file_tree.collapseAllNode();
	ExtFsDLL::releaseState();
	ExtFsDLL::closeDisks();
}
