#include "../include/node.h"
#include "../include/macro_file_type.h"
#include "../include/macro_str.h"
#include "../include/macro_constant.h"
#include "../include/macro_icon_path.h"
#include "../include/macro_gui_str.h"
#include "../include/nodearray.h"
#include "../include/extfsdll.h"
Node::~Node()
{
}
Node& Node::operator=(const Node& y)
{
	spath=y.spath;
	file_name=y.file_name;
	file_ptr=y.file_ptr;
	my_node_array=NULL;
	inode_num=y.inode_num;
	file_type=y.file_type;
	setText(0,file_name);
	if(file_type==DIR_ENTRY_FILE_TYPE_NORMAL)
		setText(1,y.text(1));
	setIcon(0,y.icon(0));
	return *this;
}
const Node& Node::copyTo(Node& y) const
{
	y.inode_num=inode_num;
	y.spath=spath;
	return y;
}
void Node::setNodeData(File* file_)
{
	inode_num=ExtFsDLL::getFileInodeNum(file_);
	file_type=ExtFsDLL::getFileType(file_);
	file_name=QString::fromUtf16(ExtFsDLL::getFileName(file_),ExtFsDLL::getFileNameLength(file_));
	setText(0,file_name);
	if(file_type==DIR_ENTRY_FILE_TYPE_NORMAL)
		setText(1,fileStrSize(ExtFsDLL::getFileSize(file_)));
	setNodeIcon();
}
void Node::setNodeIcon()
{
	if(file_type==DIR_ENTRY_FILE_TYPE_DIR)
	{
		if(dir_icon.isNull())
			dir_icon=ExtFsDLL::getDirIcon();
		setIcon(0,dir_icon);
		return;
	}
	if(file_name.indexOf(DOT)<0)
	{
		if(default_icon.isNull())
			default_icon=ExtFsDLL::getFileIcon("e");
		setIcon(0,default_icon);
		return;
	}
	QString file_type_name=file_name.section(".",-1,-1);
	for(uint32 i=0;i<Node::icons.size();i++)
	{
		if(Node::icons[i].name==file_type_name)
		{
			setIcon(0,Node::icons[i].icon);
			return;
		}
	}
	QIcon icon=ExtFsDLL::getFileIcon("."+file_type_name);
	setIcon(0,icon);
	Node::icons.append(Icon(file_type_name,icon));
}
void Node::setRootNodeData(const QString& text_str,const QString& path_str)
{
	inode_num=ROOT_INODE_NUM;
	file_type=DIR_ENTRY_FILE_TYPE_DIR;
	file_name=STR_SLASH;
	spath=path_str;
	setText(0,text_str);
	int32 index=text_str.indexOf("(");
	if(index>=0)
		setIcon(0,QIcon(WIN_FS_ICON));
	else if(text_str==UNKNOWN_DISK)
		setIcon(0,QIcon(UNKNOWN_DISK_ICON));
	else
		setIcon(0,QIcon(EXT_FS_ICON));
}
void Node::setDiskNodeData(const QString& text_str,const QString& path_str)
{
	inode_num=0;
	file_type=DIR_ENTRY_FILE_TYPE_DIR;
	file_name=text_str;
	spath=path_str;
	setText(0,text_str);
}
void Node::setPath(const QString& parent_path)
{
	spath+=parent_path;
	spath+=file_name;
	spath+=CH_SLASH;
}
void Node::cancelNodeArrayRef(){my_node_array->cancelRef();}
void Node::setNodeArrayRef(){my_node_array->setRef();}
const QString Node::fileStrSize(uint64 file_size) const
{
	double size=0.0;
	if(file_size<=1024)
		return QString::number(file_size)+"B";
	else if(file_size<=1048576)
	{
		size=(double)file_size/1024;
		return QString::number(size,'f',1)+"KB";
	}
	else if(file_size<=1073741824)
	{
		size=(double)file_size/1048576;
		return QString::number(size,'f',1)+"MB";
	}
	else
	{
		size=(double)file_size/1073741824;
		return QString::number(size,'f',1)+"GB";
	}
	return QString();
}
QList<Icon> Node::icons=QList<Icon>();
QIcon Node::dir_icon=QIcon();
QIcon Node::default_icon=QIcon();
QIcon Node::disk_icon=QIcon();