/****************************************************************************
** Meta object code from reading C++ file 'errorhandle.h'
**
** Created: Sat May 4 09:51:17 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/errorhandle.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'errorhandle.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ErrorHandle[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   13,   12,   12, 0x05,
      53,   48,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      90,   70,   12,   12, 0x08,
     123,   12,   12,   12, 0x08,
     140,   48,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ErrorHandle[] = {
    "ErrorHandle\0\0re\0handleResult(ErrorHandleResult)\0"
    "path\0dirPath(QString)\0error_report,sender\0"
    "receiveError(ErrorReport,uint32)\0"
    "showFileDialog()\0selectedDir(QString)\0"
};

void ErrorHandle::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ErrorHandle *_t = static_cast<ErrorHandle *>(_o);
        switch (_id) {
        case 0: _t->handleResult((*reinterpret_cast< const ErrorHandleResult(*)>(_a[1]))); break;
        case 1: _t->dirPath((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->receiveError((*reinterpret_cast< const ErrorReport(*)>(_a[1])),(*reinterpret_cast< uint32(*)>(_a[2]))); break;
        case 3: _t->showFileDialog(); break;
        case 4: _t->selectedDir((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ErrorHandle::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ErrorHandle::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ErrorHandle,
      qt_meta_data_ErrorHandle, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ErrorHandle::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ErrorHandle::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ErrorHandle::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ErrorHandle))
        return static_cast<void*>(const_cast< ErrorHandle*>(this));
    return QObject::qt_metacast(_clname);
}

int ErrorHandle::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void ErrorHandle::handleResult(const ErrorHandleResult & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ErrorHandle::dirPath(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
