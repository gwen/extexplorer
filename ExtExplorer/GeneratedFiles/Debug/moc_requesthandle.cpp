/****************************************************************************
** Meta object code from reading C++ file 'requesthandle.h'
**
** Created: Sat May 4 09:51:15 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/requesthandle.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'requesthandle.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RequestHandle[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      35,   15,   14,   14, 0x05,
     107,   79,   14,   14, 0x05,
     141,   14,   14,   14, 0x05,
     156,   14,   14,   14, 0x05,
     172,  169,   14,   14, 0x05,
     209,  201,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
     244,  235,   14,   14, 0x08,
     281,  271,   14,   14, 0x08,
     344,  311,   14,   14, 0x08,
     412,  409,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_RequestHandle[] = {
    "RequestHandle\0\0dir,node_array,flag\0"
    "sendNodeArray(Node,const NodeArray*,uint32)\0"
    "from_path,to_path,file_type\0"
    "copyStart(QString,QString,uint32)\0"
    "allCopyStart()\0allCopyEnd()\0pr\0"
    "copyProgress(ProgressReport)\0,sender\0"
    "error(ErrorReport,uint32)\0dir,flag\0"
    "needNodeArray(Node,uint32)\0path,flag\0"
    "needNodeArray(QString,uint32)\0"
    "files,fs_index,from_path,to_path\0"
    "needToCopyFile(QList<QTreeWidgetItem*>&,FsIndex,QString,QString)\0"
    "re\0receiveErrorResult(ErrorHandleResult)\0"
};

void RequestHandle::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RequestHandle *_t = static_cast<RequestHandle *>(_o);
        switch (_id) {
        case 0: _t->sendNodeArray((*reinterpret_cast< const Node(*)>(_a[1])),(*reinterpret_cast< const NodeArray*(*)>(_a[2])),(*reinterpret_cast< uint32(*)>(_a[3]))); break;
        case 1: _t->copyStart((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< uint32(*)>(_a[3]))); break;
        case 2: _t->allCopyStart(); break;
        case 3: _t->allCopyEnd(); break;
        case 4: _t->copyProgress((*reinterpret_cast< const ProgressReport(*)>(_a[1]))); break;
        case 5: _t->error((*reinterpret_cast< const ErrorReport(*)>(_a[1])),(*reinterpret_cast< uint32(*)>(_a[2]))); break;
        case 6: _t->needNodeArray((*reinterpret_cast< const Node(*)>(_a[1])),(*reinterpret_cast< uint32(*)>(_a[2]))); break;
        case 7: _t->needNodeArray((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< uint32(*)>(_a[2]))); break;
        case 8: _t->needToCopyFile((*reinterpret_cast< QList<QTreeWidgetItem*>(*)>(_a[1])),(*reinterpret_cast< const FsIndex(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4]))); break;
        case 9: _t->receiveErrorResult((*reinterpret_cast< const ErrorHandleResult(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData RequestHandle::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject RequestHandle::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_RequestHandle,
      qt_meta_data_RequestHandle, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RequestHandle::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RequestHandle::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RequestHandle::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RequestHandle))
        return static_cast<void*>(const_cast< RequestHandle*>(this));
    return QObject::qt_metacast(_clname);
}

int RequestHandle::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void RequestHandle::sendNodeArray(const Node & _t1, const NodeArray * _t2, uint32 _t3)const
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(const_cast< RequestHandle *>(this), &staticMetaObject, 0, _a);
}

// SIGNAL 1
void RequestHandle::copyStart(const QString & _t1, const QString & _t2, uint32 _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void RequestHandle::allCopyStart()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void RequestHandle::allCopyEnd()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void RequestHandle::copyProgress(const ProgressReport & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void RequestHandle::error(const ErrorReport & _t1, uint32 _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE
