/****************************************************************************
** Meta object code from reading C++ file 'extexplorer.h'
**
** Created: Sat May 4 09:51:17 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/extexplorer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'extexplorer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ExtExplorer[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      33,   13,   12,   12, 0x05,

 // slots: signature, parameters, type, tag, flags
      71,   59,   12,   12, 0x08,
     140,  135,   12,   12, 0x08,
     195,  135,   12,   12, 0x08,
     251,   59,   12,   12, 0x08,
     309,   12,   12,   12, 0x08,
     336,   12,   12,   12, 0x08,
     365,   12,   12,   12, 0x08,
     397,   12,   12,   12, 0x08,
     431,  428,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ExtExplorer[] = {
    "ExtExplorer\0\0error_report,sender\0"
    "error(ErrorReport,uint32)\0item,column\0"
    "on_treeWidget_file_list_itemDoubleClicked(QTreeWidgetItem*,int)\0"
    "item\0on_treeWidget_file_tree_itemExpanded(QTreeWidgetItem*)\0"
    "on_treeWidget_file_tree_itemCollapsed(QTreeWidgetItem*)\0"
    "on_treeWidget_file_tree_itemClicked(QTreeWidgetItem*,int)\0"
    "on_pushButton_up_clicked()\0"
    "on_pushButton_back_clicked()\0"
    "on_pushButton_forward_clicked()\0"
    "on_pushButton_cancel_clicked()\0re\0"
    "receiveErrorResult(ErrorHandleResult)\0"
};

void ExtExplorer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ExtExplorer *_t = static_cast<ExtExplorer *>(_o);
        switch (_id) {
        case 0: _t->error((*reinterpret_cast< const ErrorReport(*)>(_a[1])),(*reinterpret_cast< uint32(*)>(_a[2]))); break;
        case 1: _t->on_treeWidget_file_list_itemDoubleClicked((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->on_treeWidget_file_tree_itemExpanded((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 3: _t->on_treeWidget_file_tree_itemCollapsed((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 4: _t->on_treeWidget_file_tree_itemClicked((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->on_pushButton_up_clicked(); break;
        case 6: _t->on_pushButton_back_clicked(); break;
        case 7: _t->on_pushButton_forward_clicked(); break;
        case 8: _t->on_pushButton_cancel_clicked(); break;
        case 9: _t->receiveErrorResult((*reinterpret_cast< const ErrorHandleResult(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ExtExplorer::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ExtExplorer::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ExtExplorer,
      qt_meta_data_ExtExplorer, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ExtExplorer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ExtExplorer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ExtExplorer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ExtExplorer))
        return static_cast<void*>(const_cast< ExtExplorer*>(this));
    if (!strcmp(_clname, "Ui_ExtExplorerClass"))
        return static_cast< Ui_ExtExplorerClass*>(const_cast< ExtExplorer*>(this));
    return QWidget::qt_metacast(_clname);
}

int ExtExplorer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void ExtExplorer::error(const ErrorReport & _t1, uint32 _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
