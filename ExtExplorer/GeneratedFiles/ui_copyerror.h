/********************************************************************************
** Form generated from reading UI file 'copyerror.ui'
**
** Created: Sat May 25 11:27:03 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COPYERROR_H
#define UI_COPYERROR_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CopyErrorDialog
{
public:
    QFrame *frame_option;
    QGridLayout *gridLayout;
    QCheckBox *checkBox_dir;
    QCheckBox *checkBox_is_show;
    QCheckBox *checkBox_file;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *cancelButton;
    QPushButton *okButton;
    QPushButton *pushButton_option;
    QLabel *label_error_type;
    QLabel *labe_cross;
    QLabel *label_error_code;
    QLabel *label_error_type_str;
    QLabel *label_error_code_str;

    void setupUi(QDialog *CopyErrorDialog)
    {
        if (CopyErrorDialog->objectName().isEmpty())
            CopyErrorDialog->setObjectName(QString::fromUtf8("CopyErrorDialog"));
        CopyErrorDialog->resize(365, 200);
        QIcon icon;
        icon.addFile(QString::fromUtf8("icon/error.png"), QSize(), QIcon::Normal, QIcon::Off);
        CopyErrorDialog->setWindowIcon(icon);
        frame_option = new QFrame(CopyErrorDialog);
        frame_option->setObjectName(QString::fromUtf8("frame_option"));
        frame_option->setGeometry(QRect(10, 140, 341, 61));
        frame_option->setFrameShape(QFrame::StyledPanel);
        frame_option->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame_option);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        checkBox_dir = new QCheckBox(frame_option);
        checkBox_dir->setObjectName(QString::fromUtf8("checkBox_dir"));

        gridLayout->addWidget(checkBox_dir, 1, 0, 1, 1);

        checkBox_is_show = new QCheckBox(frame_option);
        checkBox_is_show->setObjectName(QString::fromUtf8("checkBox_is_show"));

        gridLayout->addWidget(checkBox_is_show, 2, 0, 1, 1);

        checkBox_file = new QCheckBox(frame_option);
        checkBox_file->setObjectName(QString::fromUtf8("checkBox_file"));

        gridLayout->addWidget(checkBox_file, 1, 1, 1, 1);

        horizontalLayoutWidget = new QWidget(CopyErrorDialog);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 100, 345, 31));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(30);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(30, 0, 30, 0);
        cancelButton = new QPushButton(horizontalLayoutWidget);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));

        horizontalLayout->addWidget(cancelButton);

        okButton = new QPushButton(horizontalLayoutWidget);
        okButton->setObjectName(QString::fromUtf8("okButton"));
        okButton->setAutoDefault(false);

        horizontalLayout->addWidget(okButton);

        pushButton_option = new QPushButton(horizontalLayoutWidget);
        pushButton_option->setObjectName(QString::fromUtf8("pushButton_option"));

        horizontalLayout->addWidget(pushButton_option);

        label_error_type = new QLabel(CopyErrorDialog);
        label_error_type->setObjectName(QString::fromUtf8("label_error_type"));
        label_error_type->setGeometry(QRect(170, 20, 151, 21));
        QFont font;
        font.setPointSize(11);
        label_error_type->setFont(font);
        label_error_type->setScaledContents(true);
        labe_cross = new QLabel(CopyErrorDialog);
        labe_cross->setObjectName(QString::fromUtf8("labe_cross"));
        labe_cross->setGeometry(QRect(20, 20, 51, 51));
        labe_cross->setPixmap(QPixmap(QString::fromUtf8("icon/error.png")));
        labe_cross->setScaledContents(true);
        label_error_code = new QLabel(CopyErrorDialog);
        label_error_code->setObjectName(QString::fromUtf8("label_error_code"));
        label_error_code->setGeometry(QRect(170, 60, 151, 21));
        label_error_code->setFont(font);
        label_error_code->setScaledContents(true);
        label_error_type_str = new QLabel(CopyErrorDialog);
        label_error_type_str->setObjectName(QString::fromUtf8("label_error_type_str"));
        label_error_type_str->setGeometry(QRect(90, 20, 81, 21));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        label_error_type_str->setFont(font1);
        label_error_type_str->setScaledContents(true);
        label_error_code_str = new QLabel(CopyErrorDialog);
        label_error_code_str->setObjectName(QString::fromUtf8("label_error_code_str"));
        label_error_code_str->setGeometry(QRect(90, 60, 81, 21));
        label_error_code_str->setFont(font1);
        label_error_code_str->setScaledContents(true);

        retranslateUi(CopyErrorDialog);
        QObject::connect(okButton, SIGNAL(clicked()), CopyErrorDialog, SLOT(accept()));
        QObject::connect(cancelButton, SIGNAL(clicked()), CopyErrorDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(CopyErrorDialog);
    } // setupUi

    void retranslateUi(QDialog *CopyErrorDialog)
    {
        CopyErrorDialog->setWindowTitle(QApplication::translate("CopyErrorDialog", "\345\244\215\345\210\266\345\207\272\351\224\231", 0, QApplication::UnicodeUTF8));
        checkBox_dir->setText(QApplication::translate("CopyErrorDialog", "\346\224\276\345\274\203\346\227\266\345\210\240\351\231\244\346\255\244\346\226\207\344\273\266\345\244\271", 0, QApplication::UnicodeUTF8));
        checkBox_is_show->setText(QApplication::translate("CopyErrorDialog", "\344\270\215\345\206\215\346\217\220\347\244\272", 0, QApplication::UnicodeUTF8));
        checkBox_file->setText(QApplication::translate("CopyErrorDialog", "\346\224\276\345\274\203\346\227\266\345\210\240\351\231\244\346\234\252\345\244\215\345\210\266\345\256\214\347\232\204\346\226\207\344\273\266", 0, QApplication::UnicodeUTF8));
        cancelButton->setText(QApplication::translate("CopyErrorDialog", "\351\207\215 \350\257\225", 0, QApplication::UnicodeUTF8));
        okButton->setText(QApplication::translate("CopyErrorDialog", "\346\224\276 \345\274\203", 0, QApplication::UnicodeUTF8));
        pushButton_option->setText(QApplication::translate("CopyErrorDialog", "\351\200\211 \351\241\271 >>", 0, QApplication::UnicodeUTF8));
        label_error_type->setText(QApplication::translate("CopyErrorDialog", "x1", 0, QApplication::UnicodeUTF8));
        labe_cross->setText(QString());
        label_error_code->setText(QApplication::translate("CopyErrorDialog", "x1", 0, QApplication::UnicodeUTF8));
        label_error_type_str->setText(QApplication::translate("CopyErrorDialog", "\351\224\231\350\257\257\347\261\273\345\236\213\357\274\232", 0, QApplication::UnicodeUTF8));
        label_error_code_str->setText(QApplication::translate("CopyErrorDialog", "\351\224\231\350\257\257\344\273\243\347\240\201\357\274\232", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CopyErrorDialog: public Ui_CopyErrorDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COPYERROR_H
