/********************************************************************************
** Form generated from reading UI file 'extexplorer.ui'
**
** Created: Sat May 25 11:26:58 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXTEXPLORER_H
#define UI_EXTEXPLORER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QFrame>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ExtExplorerClass
{
public:
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pushButton_back;
    QPushButton *pushButton_up;
    QPushButton *pushButton_forward;
    QComboBox *comboBox_fs;
    QLabel *label;
    QComboBox *comboBox_file_path;
    QPushButton *pushButton_go;
    QHBoxLayout *horizontalLayout_3;
    QTreeWidget *treeWidget_file_tree;
    QTreeWidget *treeWidget_file_list;
    QFrame *frame_copy;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_from_str;
    QLabel *label_from_path;
    QLabel *label_to_str;
    QLabel *label_to_path;
    QLabel *label_copying_str;
    QLabel *label_file_name;
    QHBoxLayout *horizontalLayout_5;
    QProgressBar *progressBar_copy;
    QPushButton *pushButton_cancel;

    void setupUi(QWidget *ExtExplorerClass)
    {
        if (ExtExplorerClass->objectName().isEmpty())
            ExtExplorerClass->setObjectName(QString::fromUtf8("ExtExplorerClass"));
        ExtExplorerClass->setWindowModality(Qt::NonModal);
        ExtExplorerClass->resize(890, 625);
        QIcon icon;
        icon.addFile(QString::fromUtf8("icon/linux.png"), QSize(), QIcon::Normal, QIcon::Off);
        ExtExplorerClass->setWindowIcon(icon);
        verticalLayout_2 = new QVBoxLayout(ExtExplorerClass);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(10);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, -1, -1);
        pushButton_back = new QPushButton(ExtExplorerClass);
        pushButton_back->setObjectName(QString::fromUtf8("pushButton_back"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pushButton_back->sizePolicy().hasHeightForWidth());
        pushButton_back->setSizePolicy(sizePolicy);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("icon/back.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_back->setIcon(icon1);
        pushButton_back->setIconSize(QSize(20, 20));

        horizontalLayout_2->addWidget(pushButton_back);

        pushButton_up = new QPushButton(ExtExplorerClass);
        pushButton_up->setObjectName(QString::fromUtf8("pushButton_up"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("icon/up.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_up->setIcon(icon2);
        pushButton_up->setIconSize(QSize(20, 20));

        horizontalLayout_2->addWidget(pushButton_up);

        pushButton_forward = new QPushButton(ExtExplorerClass);
        pushButton_forward->setObjectName(QString::fromUtf8("pushButton_forward"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8("icon/forward.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_forward->setIcon(icon3);
        pushButton_forward->setIconSize(QSize(20, 20));

        horizontalLayout_2->addWidget(pushButton_forward);

        comboBox_fs = new QComboBox(ExtExplorerClass);
        comboBox_fs->setObjectName(QString::fromUtf8("comboBox_fs"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(comboBox_fs->sizePolicy().hasHeightForWidth());
        comboBox_fs->setSizePolicy(sizePolicy1);
        QFont font;
        font.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font.setPointSize(10);
        comboBox_fs->setFont(font);
        comboBox_fs->setEditable(false);

        horizontalLayout_2->addWidget(comboBox_fs);

        label = new QLabel(ExtExplorerClass);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy2);
        QFont font1;
        font1.setPointSize(14);
        label->setFont(font1);

        horizontalLayout_2->addWidget(label);

        comboBox_file_path = new QComboBox(ExtExplorerClass);
        comboBox_file_path->setObjectName(QString::fromUtf8("comboBox_file_path"));
        sizePolicy1.setHeightForWidth(comboBox_file_path->sizePolicy().hasHeightForWidth());
        comboBox_file_path->setSizePolicy(sizePolicy1);
        comboBox_file_path->setFont(font);
        comboBox_file_path->setEditable(true);

        horizontalLayout_2->addWidget(comboBox_file_path);

        pushButton_go = new QPushButton(ExtExplorerClass);
        pushButton_go->setObjectName(QString::fromUtf8("pushButton_go"));
        QIcon icon4;
        icon4.addFile(QString::fromUtf8("icon/go.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_go->setIcon(icon4);
        pushButton_go->setIconSize(QSize(20, 20));

        horizontalLayout_2->addWidget(pushButton_go);

        horizontalLayout_2->setStretch(3, 4);
        horizontalLayout_2->setStretch(5, 16);
        horizontalLayout_2->setStretch(6, 1);

        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        treeWidget_file_tree = new QTreeWidget(ExtExplorerClass);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        treeWidget_file_tree->setHeaderItem(__qtreewidgetitem);
        treeWidget_file_tree->setObjectName(QString::fromUtf8("treeWidget_file_tree"));
        treeWidget_file_tree->setHeaderHidden(true);

        horizontalLayout_3->addWidget(treeWidget_file_tree);

        treeWidget_file_list = new QTreeWidget(ExtExplorerClass);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem();
        __qtreewidgetitem1->setText(0, QString::fromUtf8("1"));
        treeWidget_file_list->setHeaderItem(__qtreewidgetitem1);
        treeWidget_file_list->setObjectName(QString::fromUtf8("treeWidget_file_list"));
        treeWidget_file_list->setSelectionMode(QAbstractItemView::ExtendedSelection);
        treeWidget_file_list->setIndentation(20);
        treeWidget_file_list->setItemsExpandable(false);
        treeWidget_file_list->setSortingEnabled(true);
        treeWidget_file_list->setAllColumnsShowFocus(true);

        horizontalLayout_3->addWidget(treeWidget_file_list);

        horizontalLayout_3->setStretch(0, 1);
        horizontalLayout_3->setStretch(1, 2);

        verticalLayout_2->addLayout(horizontalLayout_3);

        frame_copy = new QFrame(ExtExplorerClass);
        frame_copy->setObjectName(QString::fromUtf8("frame_copy"));
        frame_copy->setEnabled(true);
        frame_copy->setFrameShape(QFrame::Box);
        frame_copy->setFrameShadow(QFrame::Sunken);
        verticalLayout = new QVBoxLayout(frame_copy);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setSizeConstraint(QLayout::SetDefaultConstraint);
        label_from_str = new QLabel(frame_copy);
        label_from_str->setObjectName(QString::fromUtf8("label_from_str"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        font2.setPointSize(10);
        font2.setBold(true);
        font2.setWeight(75);
        label_from_str->setFont(font2);

        horizontalLayout_4->addWidget(label_from_str);

        label_from_path = new QLabel(frame_copy);
        label_from_path->setObjectName(QString::fromUtf8("label_from_path"));

        horizontalLayout_4->addWidget(label_from_path);

        label_to_str = new QLabel(frame_copy);
        label_to_str->setObjectName(QString::fromUtf8("label_to_str"));
        label_to_str->setFont(font2);

        horizontalLayout_4->addWidget(label_to_str);

        label_to_path = new QLabel(frame_copy);
        label_to_path->setObjectName(QString::fromUtf8("label_to_path"));

        horizontalLayout_4->addWidget(label_to_path);

        label_copying_str = new QLabel(frame_copy);
        label_copying_str->setObjectName(QString::fromUtf8("label_copying_str"));
        label_copying_str->setFont(font2);

        horizontalLayout_4->addWidget(label_copying_str);

        label_file_name = new QLabel(frame_copy);
        label_file_name->setObjectName(QString::fromUtf8("label_file_name"));

        horizontalLayout_4->addWidget(label_file_name);

        horizontalLayout_4->setStretch(1, 4);
        horizontalLayout_4->setStretch(3, 4);
        horizontalLayout_4->setStretch(4, 1);
        horizontalLayout_4->setStretch(5, 4);

        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        progressBar_copy = new QProgressBar(frame_copy);
        progressBar_copy->setObjectName(QString::fromUtf8("progressBar_copy"));
        progressBar_copy->setValue(24);
        progressBar_copy->setAlignment(Qt::AlignCenter);
        progressBar_copy->setTextVisible(true);
        progressBar_copy->setOrientation(Qt::Horizontal);
        progressBar_copy->setInvertedAppearance(false);
        progressBar_copy->setTextDirection(QProgressBar::TopToBottom);

        horizontalLayout_5->addWidget(progressBar_copy);

        pushButton_cancel = new QPushButton(frame_copy);
        pushButton_cancel->setObjectName(QString::fromUtf8("pushButton_cancel"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("\345\256\213\344\275\223"));
        pushButton_cancel->setFont(font3);
        QIcon icon5;
        icon5.addFile(QString::fromUtf8("icon/cross.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_cancel->setIcon(icon5);

        horizontalLayout_5->addWidget(pushButton_cancel);

        horizontalLayout_5->setStretch(0, 20);
        horizontalLayout_5->setStretch(1, 1);

        verticalLayout->addLayout(horizontalLayout_5);


        verticalLayout_2->addWidget(frame_copy);


        retranslateUi(ExtExplorerClass);

        QMetaObject::connectSlotsByName(ExtExplorerClass);
    } // setupUi

    void retranslateUi(QWidget *ExtExplorerClass)
    {
        ExtExplorerClass->setWindowTitle(QApplication::translate("ExtExplorerClass", "ExtExplorer", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        pushButton_back->setToolTip(QApplication::translate("ExtExplorerClass", "\345\220\216\351\200\200", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        pushButton_back->setText(QString());
#ifndef QT_NO_TOOLTIP
        pushButton_up->setToolTip(QApplication::translate("ExtExplorerClass", "\344\270\212\344\270\200\347\272\247\347\233\256\345\275\225", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        pushButton_up->setText(QString());
#ifndef QT_NO_TOOLTIP
        pushButton_forward->setToolTip(QApplication::translate("ExtExplorerClass", "\345\211\215\350\277\233", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        pushButton_forward->setText(QString());
#ifndef QT_NO_TOOLTIP
        comboBox_fs->setToolTip(QApplication::translate("ExtExplorerClass", "ext\345\210\206\345\214\272", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("ExtExplorerClass", "\347\232\204", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        comboBox_file_path->setToolTip(QApplication::translate("ExtExplorerClass", "\345\275\223\345\211\215\350\267\257\345\276\204", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        pushButton_go->setToolTip(QApplication::translate("ExtExplorerClass", "\345\211\215\345\276\200", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        pushButton_go->setText(QString());
        label_from_str->setText(QApplication::translate("ExtExplorerClass", "\344\273\216\357\274\232", 0, QApplication::UnicodeUTF8));
        label_from_path->setText(QString());
        label_to_str->setText(QApplication::translate("ExtExplorerClass", "\345\210\260\357\274\232", 0, QApplication::UnicodeUTF8));
        label_to_path->setText(QString());
        label_copying_str->setText(QApplication::translate("ExtExplorerClass", "\346\255\243\345\234\250\345\244\215\345\210\266\357\274\232", 0, QApplication::UnicodeUTF8));
        label_file_name->setText(QString());
#ifndef QT_NO_TOOLTIP
        pushButton_cancel->setToolTip(QApplication::translate("ExtExplorerClass", "\345\217\226\346\266\210\345\244\215\345\210\266", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        pushButton_cancel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class ExtExplorerClass: public Ui_ExtExplorerClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXTEXPLORER_H
