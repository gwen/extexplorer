/****************************************************************************
** Meta object code from reading C++ file 'filelist.h'
**
** Created: Sat May 25 11:26:59 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/filelist.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'filelist.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FileList[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      29,   10,    9,    9, 0x05,
      77,   57,    9,    9, 0x05,
     141,  108,    9,    9, 0x05,
     212,  207,    9,    9, 0x05,
     240,    9,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
     282,  257,    9,    9, 0x08,
     324,    9,    9,    9, 0x08,
     339,  331,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_FileList[] = {
    "FileList\0\0dir,file_list_flag\0"
    "iNeedNodeArray(Node,uint32)\0"
    "path,file_list_flag\0iNeedNodeArray(QString,uint32)\0"
    "files,fs_index,from_path,to_path\0"
    "iNeedToCopyFile(QList<QTreeWidgetItem*>&,FsIndex,QString,QString)\0"
    "path\0showPathInComboBox(QString)\0"
    "showFileDialog()\0dir,files,file_list_flag\0"
    "listAllFile(Node,const NodeArray*,uint32)\0"
    "copy()\0to_path\0copy(QString)\0"
};

void FileList::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FileList *_t = static_cast<FileList *>(_o);
        switch (_id) {
        case 0: _t->iNeedNodeArray((*reinterpret_cast< const Node(*)>(_a[1])),(*reinterpret_cast< uint32(*)>(_a[2]))); break;
        case 1: _t->iNeedNodeArray((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< uint32(*)>(_a[2]))); break;
        case 2: _t->iNeedToCopyFile((*reinterpret_cast< QList<QTreeWidgetItem*>(*)>(_a[1])),(*reinterpret_cast< const FsIndex(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4]))); break;
        case 3: _t->showPathInComboBox((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->showFileDialog(); break;
        case 5: _t->listAllFile((*reinterpret_cast< const Node(*)>(_a[1])),(*reinterpret_cast< const NodeArray*(*)>(_a[2])),(*reinterpret_cast< uint32(*)>(_a[3]))); break;
        case 6: _t->copy(); break;
        case 7: _t->copy((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData FileList::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FileList::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FileList,
      qt_meta_data_FileList, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FileList::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FileList::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FileList::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FileList))
        return static_cast<void*>(const_cast< FileList*>(this));
    return QObject::qt_metacast(_clname);
}

int FileList::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void FileList::iNeedNodeArray(const Node & _t1, uint32 _t2)const
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(const_cast< FileList *>(this), &staticMetaObject, 0, _a);
}

// SIGNAL 1
void FileList::iNeedNodeArray(const QString & _t1, uint32 _t2)const
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(const_cast< FileList *>(this), &staticMetaObject, 1, _a);
}

// SIGNAL 2
void FileList::iNeedToCopyFile(QList<QTreeWidgetItem*> & _t1, const FsIndex & _t2, const QString & _t3, const QString & _t4)const
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)) };
    QMetaObject::activate(const_cast< FileList *>(this), &staticMetaObject, 2, _a);
}

// SIGNAL 3
void FileList::showPathInComboBox(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void FileList::showFileDialog()const
{
    QMetaObject::activate(const_cast< FileList *>(this), &staticMetaObject, 4, 0);
}
QT_END_MOC_NAMESPACE
