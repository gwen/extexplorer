/****************************************************************************
** Meta object code from reading C++ file 'progress.h'
**
** Created: Sat May 25 11:26:59 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../include/progress.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'progress.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Progress[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      38,   10,    9,    9, 0x08,
      68,    9,    9,    9, 0x08,
      76,    9,    9,    9, 0x08,
      85,   82,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Progress[] = {
    "Progress\0\0from_path,to_path,file_type\0"
    "start(QString,QString,uint32)\0start()\0"
    "end()\0pr\0receiveProgress(ProgressReport)\0"
};

void Progress::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Progress *_t = static_cast<Progress *>(_o);
        switch (_id) {
        case 0: _t->start((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< uint32(*)>(_a[3]))); break;
        case 1: _t->start(); break;
        case 2: _t->end(); break;
        case 3: _t->receiveProgress((*reinterpret_cast< const ProgressReport(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Progress::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Progress::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Progress,
      qt_meta_data_Progress, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Progress::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Progress::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Progress::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Progress))
        return static_cast<void*>(const_cast< Progress*>(this));
    return QObject::qt_metacast(_clname);
}

int Progress::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
