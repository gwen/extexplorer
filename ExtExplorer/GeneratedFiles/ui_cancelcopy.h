/********************************************************************************
** Form generated from reading UI file 'cancelcopy.ui'
**
** Created: Sat May 25 11:27:03 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CANCELCOPY_H
#define UI_CANCELCOPY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CancelCopyDialog
{
public:
    QFrame *frame_option;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout;
    QCheckBox *checkBox_del_dir;
    QCheckBox *checkBox_del_all;
    QCheckBox *checkBox_is_show;
    QCheckBox *checkBox_cancel_other;
    QCheckBox *checkBox_cancel_all;
    QCheckBox *checkBox_del_incomp;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *okButton;
    QPushButton *cancelButton;
    QPushButton *pushButton_option;
    QLabel *label_hint;
    QLabel *label_exclamation;

    void setupUi(QDialog *CancelCopyDialog)
    {
        if (CancelCopyDialog->objectName().isEmpty())
            CancelCopyDialog->setObjectName(QString::fromUtf8("CancelCopyDialog"));
        CancelCopyDialog->resize(450, 230);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CancelCopyDialog->sizePolicy().hasHeightForWidth());
        CancelCopyDialog->setSizePolicy(sizePolicy);
        CancelCopyDialog->setMinimumSize(QSize(0, 0));
        CancelCopyDialog->setMaximumSize(QSize(10000, 10000));
        QIcon icon;
        icon.addFile(QString::fromUtf8("icon/exclamation.png"), QSize(), QIcon::Normal, QIcon::Off);
        CancelCopyDialog->setWindowIcon(icon);
        CancelCopyDialog->setSizeGripEnabled(false);
        CancelCopyDialog->setModal(true);
        frame_option = new QFrame(CancelCopyDialog);
        frame_option->setObjectName(QString::fromUtf8("frame_option"));
        frame_option->setGeometry(QRect(10, 130, 431, 91));
        frame_option->setFrameShape(QFrame::NoFrame);
        frame_option->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(frame_option);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setHorizontalSpacing(3);
        gridLayout->setVerticalSpacing(2);
        checkBox_del_dir = new QCheckBox(frame_option);
        checkBox_del_dir->setObjectName(QString::fromUtf8("checkBox_del_dir"));

        gridLayout->addWidget(checkBox_del_dir, 1, 0, 1, 1);

        checkBox_del_all = new QCheckBox(frame_option);
        checkBox_del_all->setObjectName(QString::fromUtf8("checkBox_del_all"));

        gridLayout->addWidget(checkBox_del_all, 1, 1, 1, 1);

        checkBox_is_show = new QCheckBox(frame_option);
        checkBox_is_show->setObjectName(QString::fromUtf8("checkBox_is_show"));

        gridLayout->addWidget(checkBox_is_show, 1, 2, 1, 1);

        checkBox_cancel_other = new QCheckBox(frame_option);
        checkBox_cancel_other->setObjectName(QString::fromUtf8("checkBox_cancel_other"));
        checkBox_cancel_other->setCheckable(true);

        gridLayout->addWidget(checkBox_cancel_other, 0, 0, 1, 1);

        checkBox_cancel_all = new QCheckBox(frame_option);
        checkBox_cancel_all->setObjectName(QString::fromUtf8("checkBox_cancel_all"));
        checkBox_cancel_all->setCheckable(true);

        gridLayout->addWidget(checkBox_cancel_all, 0, 1, 1, 1);

        checkBox_del_incomp = new QCheckBox(frame_option);
        checkBox_del_incomp->setObjectName(QString::fromUtf8("checkBox_del_incomp"));

        gridLayout->addWidget(checkBox_del_incomp, 0, 2, 1, 1);


        horizontalLayout->addLayout(gridLayout);

        horizontalLayoutWidget_2 = new QWidget(CancelCopyDialog);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(20, 90, 411, 25));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_3->setSpacing(50);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(30, 0, 30, 0);
        okButton = new QPushButton(horizontalLayoutWidget_2);
        okButton->setObjectName(QString::fromUtf8("okButton"));

        horizontalLayout_3->addWidget(okButton);

        cancelButton = new QPushButton(horizontalLayoutWidget_2);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));

        horizontalLayout_3->addWidget(cancelButton);

        pushButton_option = new QPushButton(horizontalLayoutWidget_2);
        pushButton_option->setObjectName(QString::fromUtf8("pushButton_option"));

        horizontalLayout_3->addWidget(pushButton_option);

        label_hint = new QLabel(CancelCopyDialog);
        label_hint->setObjectName(QString::fromUtf8("label_hint"));
        label_hint->setGeometry(QRect(100, 20, 191, 41));
        QFont font;
        font.setPointSize(15);
        label_hint->setFont(font);
        label_hint->setTextFormat(Qt::AutoText);
        label_hint->setScaledContents(false);
        label_exclamation = new QLabel(CancelCopyDialog);
        label_exclamation->setObjectName(QString::fromUtf8("label_exclamation"));
        label_exclamation->setGeometry(QRect(30, 10, 81, 61));
        label_exclamation->setPixmap(QPixmap(QString::fromUtf8("icon/exclamation.png")));
        label_exclamation->setScaledContents(true);

        retranslateUi(CancelCopyDialog);
        QObject::connect(okButton, SIGNAL(clicked()), CancelCopyDialog, SLOT(accept()));
        QObject::connect(cancelButton, SIGNAL(clicked()), CancelCopyDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(CancelCopyDialog);
    } // setupUi

    void retranslateUi(QDialog *CancelCopyDialog)
    {
        CancelCopyDialog->setWindowTitle(QApplication::translate("CancelCopyDialog", "\347\241\256\350\256\244\345\217\226\346\266\210", 0, QApplication::UnicodeUTF8));
        checkBox_del_dir->setText(QApplication::translate("CancelCopyDialog", "\345\210\240\351\231\244\345\267\262\345\244\215\345\210\266\347\232\204\346\226\207\344\273\266\345\244\271", 0, QApplication::UnicodeUTF8));
        checkBox_del_all->setText(QApplication::translate("CancelCopyDialog", "\345\210\240\351\231\244\345\267\262\345\244\215\345\210\266\347\232\204\345\205\250\351\203\250\346\226\207\344\273\266", 0, QApplication::UnicodeUTF8));
        checkBox_is_show->setText(QApplication::translate("CancelCopyDialog", "\344\270\215\345\206\215\346\217\220\347\244\272", 0, QApplication::UnicodeUTF8));
        checkBox_cancel_other->setText(QApplication::translate("CancelCopyDialog", "\345\217\226\346\266\210\345\244\215\345\210\266\345\205\266\344\273\226\346\226\207\344\273\266", 0, QApplication::UnicodeUTF8));
        checkBox_cancel_all->setText(QApplication::translate("CancelCopyDialog", "\345\217\226\346\266\210\345\244\215\345\210\266\346\211\200\346\234\211\346\226\207\344\273\266", 0, QApplication::UnicodeUTF8));
        checkBox_del_incomp->setText(QApplication::translate("CancelCopyDialog", "\345\210\240\351\231\244\346\234\252\345\244\215\345\210\266\345\256\214\347\232\204\346\226\207\344\273\266", 0, QApplication::UnicodeUTF8));
        okButton->setText(QApplication::translate("CancelCopyDialog", "\346\230\257", 0, QApplication::UnicodeUTF8));
        cancelButton->setText(QApplication::translate("CancelCopyDialog", "\345\220\246", 0, QApplication::UnicodeUTF8));
        pushButton_option->setText(QApplication::translate("CancelCopyDialog", "\351\200\211 \351\241\271 >>", 0, QApplication::UnicodeUTF8));
        label_hint->setText(QApplication::translate("CancelCopyDialog", "\347\241\256\345\256\232\345\217\226\346\266\210\345\244\215\345\210\266\346\223\215\344\275\234\357\274\237", 0, QApplication::UnicodeUTF8));
        label_exclamation->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class CancelCopyDialog: public Ui_CancelCopyDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CANCELCOPY_H
