#ifndef MACRO_SUPERBLOCK_H
#define MACRO_SUPERBLOCK_H
#include "type.h"
#include <math.h>

#define SUPER_BLOCK_SIZE 1024 
#define BLOCK_SIZE(x) (uint32)pow(2,(double)*(uint32*)((x)+0x18))*1024 
#define INODE_SIZE(x) (uint16)(*(uint32*)((x)+0x4C)!=0?*(uint16*)((x)+0x58):128) 
#define INODE_COUNT_PER_GROUP(x) *(uint32*)((x)+0x28)
#define GROUP_DESC_START(x) (uint32)(blockSize()!=1024?1:2)
#define DESCS_SIZE(x) (uint32)(*(uint16*)((x)+0xFE)==0?32:64);
#define TOTAL_BLOCK_COUNT_LOW(x) *((uint32*)(x)+1);
#define TOTAL_BLOCK_COUNT_HIGH(x) *((uint32*)(x)+84);
#define EXTFS_NAME(x) ((x)+0x78) 
#define EXTFS_NAME_LENGTH 16

#endif // MACRO_SUPERBLOCK_H