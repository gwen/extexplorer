#ifndef DISK_H
#define DISK_H
#include <windows.h>
#include "type.h"
#include "ptrarray.h"
class FS;
class PartitionInfo 
{
	friend class Disk;
private:
	PartitionInfo(uint32 _start_sector,uint8 _fs_type):start_sector(_start_sector),fs_type(_fs_type){}
	uint32 start_sector;
	uint8 fs_type;
};
class Disk
{
public:
	Disk();
	Disk(HANDLE _disk_handle,uint32 win_disk_indx_);
	FS& operator[](uint32 index) const{return fs_array[index];}
	uint32 fsCount() const{return fs_array.size();}
	uint32 winDiskIndex() const{return win_disk_index;}
	~Disk();
private:
	PtrArray<PartitionInfo> scanPartitionTable() const;
	PtrArray<FS> getFS(PtrArray<PartitionInfo>& ifs) const;
	HANDLE disk_handle;
	PtrArray<FS> fs_array;
	uint32 win_disk_index;
};
#endif // DISK_H
