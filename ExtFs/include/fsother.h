#ifndef FSOTHER_H
#define FSOTHER_H
#include "fs.h"
#include "type.h"
#include "state.h"
#include "macro_report.h"
class File;
template <typename T> class PtrArray;
class FsOther:public FS
{
public:
	FsOther(uint32 start,uint8 type,HANDLE handle):FS(start,type,handle){}

	bool copy(const wint16* from_path,const wint16* to_path) const
	{
		State::state().reportError(ERROR_TYPE_NOT_EXT_FS,0);
		return false;
	}
	bool copy(uint32 inode_num,const wint16* to_path) const
	{
		State::state().reportError(ERROR_TYPE_NOT_EXT_FS,0);
		return false;
	}
	PtrArray<File> scanDir(uint32 inode_num) const
	{
		State::state().reportError(ERROR_TYPE_NOT_EXT_FS,0);
		return PtrArray<File>();
	}
	PtrArray<File> scanDir(const wint16* path) const
	{
		State::state().reportError(ERROR_TYPE_NOT_EXT_FS,0);
		return PtrArray<File>();
	}
	bool checkDir(uint32 inode_num) const
	{
		State::state().reportError(ERROR_TYPE_NOT_EXT_FS,0);
		return false;
	}
	bool checkDir(const wint16* path) const
	{
		State::state().reportError(ERROR_TYPE_NOT_EXT_FS,0);
		return false;
	}

};
#endif