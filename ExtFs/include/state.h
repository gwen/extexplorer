#ifndef STATE_H
#define STATE_H
#include "type.h"
#include "winapi.h"
class ErrorReport
{
	friend class State;
private:
	ErrorReport():error_type(0),error_code(0){}
	ErrorReport(uint32 _error_type,uint32 _error_code):error_type(_error_type),error_code(_error_code){}
	volatile uint32 error_type;
	volatile uint32 error_code;
};
class ProgressReport
{
	friend class State;
private:
	ProgressReport():progress_type(0),progress_value(0),name(NULL),name_length(0){}
	ProgressReport(uint32 p_type,uint32 p_value,const uint16* name_,uint32 name_length_):
	progress_type(p_type),progress_value(p_value),name(name_),name_length(name_length_){}
	uint32 progress_type;
	uint32 progress_value;
	const uint16* name;
	volatile uint32 name_length;
};
class State
{
	friend EXTFS_DLL_EXPORT void dll_initState();
	friend EXTFS_DLL_EXPORT void dll_releaseState();
public:
	static State& state(){return *state_ptr;}

	uint32 reportType(){return report_type;}
	void reportBlockCount(uint32 prog_type,uint32 blk_count);
	void reportFileName(uint32 prog_type,const uint16* name,uint32 name_length);
	void reportError(uint32 error_type,uint32 error_code);
	void errorReport(ErrorReport& report) const;
	void progressReport(ProgressReport& report);
	void cancelCopy(bool is_cancel_other,bool is_del_file_);
	void continueCopy();
	void suspendCopy(){is_cancel_copy=true;}
	void checkCancel();
	void startWatching(){event_for_watch.lock();is_have_watch=true;}
	void watching(){event_for_watch.lock();}
	void endWatching(){event_for_watch.unlock();is_have_watch=false;} 
	bool isHaveWatch(){return is_have_watch;}
	uint32 isRetry(){return is_retry;}
    void stopWatching();
	void retry(uint32 is_retry_);
	void occurCopyError(uint32 error_type,uint32 error_code);
	void initState(){report_type=0;is_cancel_copy=false;is_overwrite=false;copyed_block=0;}
	bool isOverwrite(){return is_overwrite;}
	void overwrite(bool is_over_write_);
private:
	static void createState();
	State():report_type(0),is_cancel_copy(false),is_cancel_other(true),is_overwrite(false),
		is_del_file(true),is_have_watch(false){}
	uint32 report_type;
	ErrorReport error_report;
	ProgressReport prog_report;
	uint32 copyed_block;
	volatile bool is_cancel_copy;
	volatile bool is_cancel_other;
	volatile bool is_del_file;
	volatile bool is_overwrite;
	volatile bool is_have_watch;
	volatile uint32 is_retry;
	static State* state_ptr;
	Lock lock_for_error_report;
	WaitEvent event_for_watch;
};

#endif //STATE_H