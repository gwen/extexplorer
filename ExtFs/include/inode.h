#ifndef INODE_H
#define INODE_H
#include "type.h"
#include "smartptr.h"
#include "macro_inode.h"
struct InodeDataPack
{
	friend class Inode;
private:
	uint64 file_size;
	bool is_use_extent;
	uint16 file_mode;
	uint8 i_block[INODE_BLOCK_ADDR_BYTE];
};
class Inode
{
public:
	Inode();
	Inode(uint8* inode_start);
	Inode(const Inode& y);
	Inode& operator=(const Inode& y);
	uint32 fileType() const;
	const uint8* blockPtrs() const
	{
		return sptr->data_pack->i_block;
	}
	bool isNull() const
	{
		return sptr==NULL;
	}
	bool isUseExtent() const
	{
		return sptr->data_pack->is_use_extent;
	}
	const uint8* extentPtr() const
	{
		return sptr->data_pack->i_block;
	}
	uint64 fileSize() const
	{
		return sptr->data_pack->file_size;
	}
	~Inode();
private:
	SmartPtr<uint8,InodeDataPack>* sptr;
};
#endif