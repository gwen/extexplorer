#ifndef SMARTPTR_H
#define SMARTPTR_H
#include "type.h"
template <typename T,typename TT=uint32>
class SmartPtr
{
public:
	SmartPtr(){}
	SmartPtr(T* ptr,TT* pp=NULL):times(1),space(ptr),data_pack(pp){}
	~SmartPtr()
	{
		delete[] space;
		delete data_pack;
	}
	uint32 times;
	T* space;
	TT* data_pack; 
};


#endif // SMARTPTR_H