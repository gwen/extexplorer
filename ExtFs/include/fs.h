#ifndef FS_H
#define FS_H
#include <windows.h>
#include "type.h"
#include "buffer.h"
class File;
template <typename T> class PtrArray;
class FS
{
public:
    FS(){}
    FS(uint32 start,uint8 type,HANDLE handle):start_sector(start),disk_handle(handle),fs_type(type){}
	virtual bool copy(const wint16* from_path,const wint16* to_path) const=0;
	virtual bool copy(uint32 inode_num,const wint16* to_path) const=0;
	virtual PtrArray<File> scanDir(uint32 inode_num) const=0;
	virtual PtrArray<File> scanDir(const wint16* path) const=0;
	virtual bool checkDir(uint32 inode_num) const=0;
	virtual bool checkDir(const wint16* path) const=0;
	virtual ~FS(){}
    void setFsName(const Buffer& name){fs_name=name;}
	uint16* fsName() const{return (uint16*)fs_name.data();}
	uint32 fsNameLength() const{return fs_name.size()>>1;}
protected:
    uint32 start_sector;
    HANDLE disk_handle;
	uint8 fs_type;
	Buffer fs_name;

	
};

#endif // FS_H
