#ifndef FILE_H
#define FILE_H
#include "type.h"
#include "direntry.h"
#include "inode.h"
class File
{
	friend class FsExt;
public:
	File(){}
	File(uint8* dir_entry_start):dir_entry(dir_entry_start),inode(),sub_dir(){}
	uint64 size() const
	{
		return inode.fileSize();
	}
	const uint16* name() const
	{
		return (uint16*)dir_entry.file_name.data();
	}
	uint32 nameSize() const
	{
		return dir_entry.file_name.size()>>1;
	}
	uint32 type() const
	{
		return dir_entry.type;
	}
	uint32 inodeNum() const {return dir_entry.inode_num;}
	~File(){}
private:
	DirEntry dir_entry; 
	Inode inode; 
	PtrArray<File> sub_dir;
};
#endif //FILE_H