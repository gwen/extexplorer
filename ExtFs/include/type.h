#ifndef TYPE_H
#define TYPE_H
#ifndef NULL
#define NULL 0
#endif
#define EXTFS_DLL_EXPORT _declspec(dllexport)
typedef unsigned char uint8;
typedef char int8;
typedef unsigned short int uint16;
typedef wchar_t wint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;
typedef short int int16;
typedef int int32;
typedef long long int64;


#endif // Z_TYPE_H
