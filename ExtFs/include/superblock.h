#ifndef SUPTERBLOCK_H
#define SUPTERBLOCK_H

#include "type.h"
#include "buffer.h"
#include "macro_superblock.h"
class SuperBlock
{
public:
	SuperBlock():block(SUPER_BLOCK_SIZE){}
	~SuperBlock(){}
	uint32 blockSize()
	{
		return BLOCK_SIZE(block.data());
	}
	uint16 inodeSize()
	{
		return INODE_SIZE(block.data());
	}
	uint32 inodesPerGroup()
	{
		return INODE_COUNT_PER_GROUP(block.data());
	}
	uint32 groupDescStart()
	{
		return GROUP_DESC_START(block.data());
	}
	uint32 descSize()
	{
		return DESCS_SIZE(block.data());
	}
	uint32 descsPerBlock()
	{
		return blockSize()/descSize();
	}
	uint32 totalBlockCountLow()
	{
		return TOTAL_BLOCK_COUNT_LOW(block.data());
	}
	uint32 totalBlockCountHigh()
	{
		return TOTAL_BLOCK_COUNT_HIGH(block.data());
	}
	const Buffer fsName();
	Buffer block;
};
#endif //SUPTERBLOCK_H


