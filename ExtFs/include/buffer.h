#ifndef BUFFER_H
#define BUFFER_H
#include "type.h"
class Buffer
{
public:
	Buffer():data_ptr(NULL),data_size(0),is_destroy(true){}
	Buffer(uint32 csize):data_size(csize),is_destroy(true),data_ptr(new uint8[csize]){}
	Buffer(uint8* ptr,uint32 csize,bool is_destroy_=false):data_size(csize),data_ptr(ptr),is_destroy(is_destroy_){}
	void operator()(uint8* ptr,uint32 csize)
	{
		data_ptr=ptr;
		data_size=csize;
		is_destroy=true;
	}
	bool operator==(const Buffer& x) const;
	void operator=(const Buffer& x);
	uint8 operator[](uint32 index)
	{
		return *(data_ptr+index);
	}
	~Buffer();
	uint8* data() const{return data_ptr;}
	uint32 size() const{return data_size;}
private:
	uint32 data_size;
	uint8* data_ptr;
	bool is_destroy;
};
#endif // BUFFER_H
