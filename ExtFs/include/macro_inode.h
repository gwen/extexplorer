#ifndef MACRO_INODE_H
#define MACRO_INODE_H
#include "type.h"
#define INODE_BLOCK_ADDR_COUNT 15
#define INODE_BLOCK_ADDR_BYTE 60
#define INODE_BLOCK_ADDR_START(x) (uint32*)((x)+0x28)
#define INODE_FLAGS(x) *(uint32*)((x)+0x20)
#define INODE_MODE(x) *(uint16*)(x)
#define INODE_FILE_SIZE(x) ((uint64)*(uint32*)((x)+ 0x6c)<<32)+(uint64)*(uint32*)((x)+ 0x04)
#endif // MACRO_INODE_H