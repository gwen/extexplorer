#ifndef MACRO_PARTITION_H
#define MACRO_PARTITION_H
#include "type.h"
#define TABLE_START 0x01BE
#define ENTRY_SIZE 16
#define ENTRY_COUNT 4

#define FS_TYPE_LINUX 0x83
#define FS_TYPE_EXTEND1 0x05 
#define FS_TYPE_EXTEND2 0x0F 
#define FS_TYPE(x,n) *((x)+TABLE_START+(n)*ENTRY_SIZE+4)
#define START_SCETOR(x,n) *(uint32*)((x)+TABLE_START+(n)*ENTRY_SIZE+8)



#endif // MACRO_PARTITION_H