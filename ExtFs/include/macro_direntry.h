#ifndef MACRO_DIRENTRY_H
#define MACRO_DIRENTRY_H
#include "type.h"
#define INODE_NUM(x) *(uint32*)(x)
#define ENTRY_LENGTH(x) *(uint16*)((x)+0x04)
#define NAME_LENGTH(x) *((x)+0x06)
#define TYPE(x) *((x)+0x07)
#define NAME_PTR(x) ((x)+0x08)
#define FIRST_DEFAULT_ENTRY_LENGTH 12
#define SECOND_DEFAULT_ENTRY_LENGTH(x) ENTRY_LENGTH((x)+FIRST_DEFAULT_ENTRY_LENGTH)
#define HAVE_FIRST_DEFAULT_ENTRY(x) (NAME_LENGTH((x))==1)&&(*NAME_PTR((x))=='.') 

#endif // MACRO_DIRENTRY_H