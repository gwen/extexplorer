#ifndef DIRENTRY_H
#define DIRENTRY_H
#include "type.h"
#include "buffer.h"
class DirEntry
{
public:
	DirEntry();
	DirEntry(uint8* dir_entry_start);
	~DirEntry();
	uint32 type;
	uint32 inode_num;
	Buffer file_name;
};
#endif //DIRENTRY_H
