#ifndef PTRARRAY_H
#define PTRARRAY_H
#include "type.h"
#include "smartptr.h"
template <typename T> class PtrList;
template <typename T>

class PtrArray
{
	friend class PtrList<T>;
public:
	PtrArray():sptr(NULL),m_size(0){}
	PtrArray(uint32 csize):m_size(csize)
	{
		sptr=new SmartPtr<T*>(new T*[csize]);
	}
	PtrArray(const PtrArray& y)
	{
		if(y.sptr!=NULL)
			++y.sptr->times;
		sptr=y.sptr;
		m_size=y.m_size;
	}
	~PtrArray()
	{
		if(sptr!=NULL&&--sptr->times==0)
		{
			for(uint32 i=0;i<m_size;i++)
				delete *(sptr->space+i);
			delete sptr;
		}
	}
	T& operator[](uint32 index) const
	{
		return **(sptr->space+index);
	}
	T* at(uint32 index)  const
	{
		return *(sptr->space+index);
	}
	PtrArray<T>& operator=(const PtrArray& y)
	{
		if(y.sptr!=NULL)
			++y.sptr->times;
		if(sptr!=NULL&&--sptr->times==0)
		{
			for(uint32 i=0;i<m_size;i++)
				delete *(sptr->space+i);
			delete sptr;
		}
		sptr=y.sptr;
		m_size=y.m_size;
		return *this;
	}
	void setPtr(T* y,uint32 i)
	{
		*(sptr->space+i)=y;
	}
	uint32 size() const{return m_size;}
private:
	uint32 m_size;
	SmartPtr<T*>* sptr;
};
#endif  //PTRARRAY_H
