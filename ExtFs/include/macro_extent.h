#ifndef MACRO_EXTENT_H
#define MACRO_EXTENT_H
#include "type.h"
#define EXTENT_SIZE 12 
#define EXTENT_COUNT(x) *(uint16*)((x)+2)
#define DEPTH_OF_TREE(x) *(uint16*)((x)+6)
#define BLOCK_NUM_IN_EXTENT_IDX(x) ((uint64)*(uint16*)((x)+8)<<32)+(uint64)*(uint32*)((x)+4)
#define BLOCK_NUM_IN_EXTENT(x) ((uint64)*(uint16*)((x)+6)<<32)+(uint64)*(uint32*)((x)+8)
#define BLOCK_COUNT_IN_EXTENT(x) *(uint16*)((x)+4)

#endif // MACRO_EXTENT_H