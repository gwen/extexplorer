#ifndef OPENDISK_H
#define OPENDISK_H
#include "ptrarray.h"
#include <windows.h>
class Disk;
class File;
class Disks
{
	friend EXTFS_DLL_EXPORT bool dll_openDisks();
	friend EXTFS_DLL_EXPORT void dll_closeDisks();
public:
	static Disks& disks(){return *disks_ptr;}
	uint32 diskCount() const{return disk_array.size();}
	Disk& operator[](uint32 index) const{return disk_array[index];}
private:
	Disks();
	PtrArray<Disk> getEveryThing();
	void setWindowsFsName();
	PtrArray<Disk> disk_array;
	static Disks* disks_ptr;
};

#endif // OPENDISK_H
