#ifndef EXCEPTION_H
#define EXCEPTION_H
#include "type.h"
class ExpWinAPI
{
public:
	ExpWinAPI():code(0),type(0),is_del_file(false){}
	ExpWinAPI(uint32 error_type,uint32 error_code,bool idf=false):code(error_code),type(error_type),is_del_file(idf){}
	uint32 code;
	uint32 type;
	bool is_del_file; 
};
class ExpCancel 
{
public:
	ExpCancel(uint32 _type,bool is_throw_up_,bool is_del_file_):type(_type),
		is_throw_up(is_throw_up_),is_del_file(is_del_file_){}
	bool is_throw_up;
	bool is_del_file; 
	uint32 type;
};

#endif // EXCEPTION_H