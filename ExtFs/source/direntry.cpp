#include "../include/direntry.h"
#include "../include/winapi.h"
#include "../include/macro_direntry.h"
DirEntry::DirEntry(){}
DirEntry::DirEntry(uint8* dir_entry_start)
{
	uint8* name=NAME_PTR(dir_entry_start);
	uint32 name_length=NAME_LENGTH(dir_entry_start);
	type=TYPE(dir_entry_start);
	inode_num=INODE_NUM(dir_entry_start);
	WinAPI::utf8ToUtf16(name,name_length,file_name);
}
DirEntry::~DirEntry(){}
