#include "../include/inode.h"
#include "../include/macro_file_type.h"
#include "../include/macro_inode.h"
Inode::Inode():sptr(NULL){}
Inode::Inode(uint8* inode_start)
{
	sptr=new SmartPtr<uint8,InodeDataPack>(NULL,new InodeDataPack);
	for (uint32 i=0; i<INODE_BLOCK_ADDR_COUNT; i++)
		*((uint32*)(sptr->data_pack->i_block)+i)=*(INODE_BLOCK_ADDR_START(inode_start)+i);
	sptr->data_pack->is_use_extent=(INODE_FLAGS(inode_start)&0x80000)==0x80000;
	sptr->data_pack->file_mode=INODE_MODE(inode_start)&0xF000;
	sptr->data_pack->file_size=INODE_FILE_SIZE(inode_start);
}
Inode::Inode(const Inode& y)
{
	if(y.sptr!=NULL)
		++y.sptr->times;
	sptr=y.sptr;
}
Inode& Inode::operator=(const Inode& y)
{
	if(y.sptr!=NULL)
		++y.sptr->times;
	if(sptr!=NULL&&--sptr->times==0)
		delete sptr;
	sptr=y.sptr;
	return *this;
}
Inode::~Inode()
{
	if(sptr!=NULL&&--sptr->times==0)
		delete sptr;
}
uint32 Inode::fileType() const
{
	switch (sptr->data_pack->file_mode)
	{
	case INODE_FILE_TYPE_FIFO:
		return INODE_FILE_TYPE_FIFO;
	case INODE_FILE_TYPE_CHAR:
		return INODE_FILE_TYPE_CHAR;
	case INODE_FILE_TYPE_DIR:
		return INODE_FILE_TYPE_DIR;
	case INODE_FILE_TYPE_BLOCK:
		return INODE_FILE_TYPE_BLOCK;
	case INODE_FILE_TYPE_NORMAL:
		return INODE_FILE_TYPE_NORMAL;
	case INODE_FILE_TYPE_LINK:
		return INODE_FILE_TYPE_LINK;
	case INODE_FILE_TYPE_SOCKET:
		return INODE_FILE_TYPE_SOCKET;
	default:
		return 0;
	}
}