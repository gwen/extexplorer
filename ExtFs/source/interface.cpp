#include <windows.h>
#include "../include/state.h"
#include "../include/disks.h"
#include "../include/ptrarray.h"
#include "../include/file.h"
#include "../include/disk.h"
#include "../include/fs.h"
EXTFS_DLL_EXPORT bool dll_openDisks()
{
	if(Disks::disks_ptr==NULL)
	{
		Disks::disks_ptr=new Disks();
		if(Disks::disks_ptr->disk_array.size()==0)
		{
			delete Disks::disks_ptr;
			Disks::disks_ptr=NULL;
			return true;
		}
	}
	return false;
}
EXTFS_DLL_EXPORT uint32 dll_diskCount()
{
	return Disks::disks().diskCount();
}
EXTFS_DLL_EXPORT uint32 dll_partitionCount(uint32 disk_index)
{
	return Disks::disks()[disk_index].fsCount();
}
EXTFS_DLL_EXPORT void dll_closeDisks(){delete Disks::disks_ptr;}
EXTFS_DLL_EXPORT uint16* dll_fsName(uint32 disk_index,uint32 fs_index)
{
	return Disks::disks()[disk_index][fs_index].fsName();
}
EXTFS_DLL_EXPORT uint32 dll_fsNameLength(uint32 disk_index,uint32 fs_index)
{
	return Disks::disks()[disk_index][fs_index].fsNameLength();
}


EXTFS_DLL_EXPORT void dll_initState(){State::createState();}
EXTFS_DLL_EXPORT void dll_releaseState(){delete State::state_ptr;}


EXTFS_DLL_EXPORT void dll_suspendCopy(){State::state().suspendCopy();}
EXTFS_DLL_EXPORT void dll_cancelCopy(bool is_cancel_all,bool is_del_file)
{
	State::state().cancelCopy(is_cancel_all,is_del_file);
}
EXTFS_DLL_EXPORT void dll_continueCopy(){State::state().continueCopy();}


EXTFS_DLL_EXPORT void dll_startWatching(){State::state().startWatching();}
EXTFS_DLL_EXPORT void dll_watching(){State::state().watching();}
EXTFS_DLL_EXPORT void dll_stopWatching(){State::state().stopWatching();}
EXTFS_DLL_EXPORT void dll_endWatching(){return State::state().endWatching();}
EXTFS_DLL_EXPORT void dll_retry(uint32 is_retry){State::state().retry(is_retry);}
EXTFS_DLL_EXPORT void dll_overwrite(bool is_over_write_){State::state().overwrite(is_over_write_);}


EXTFS_DLL_EXPORT uint32 dll_getReportType(){return State::state().reportType();}
EXTFS_DLL_EXPORT void dll_getErrorReport(ErrorReport& report){State::state().errorReport(report);}
EXTFS_DLL_EXPORT void dll_getProgressReport(ProgressReport& report){State::state().progressReport(report);}


EXTFS_DLL_EXPORT const uint16* dll_getFileName(File* file){return file->name();}
EXTFS_DLL_EXPORT uint32 dll_getFileNameLength(File* file){return file->nameSize();}
EXTFS_DLL_EXPORT uint64 dll_getFileSize(File* file){return file->size();}
EXTFS_DLL_EXPORT uint32 dll_getFileType(File* file){return file->type();}
EXTFS_DLL_EXPORT uint32 dll_getFileInodeNum(File* file){return file->inodeNum();}
EXTFS_DLL_EXPORT File* dll_getFilePtr(PtrArray<File>* files,uint32 index){return files->at(index);}
EXTFS_DLL_EXPORT uint32 dll_getFileCount(PtrArray<File>* files){return files->size();}


EXTFS_DLL_EXPORT PtrArray<File>* dll_scanDirByInodeNum(uint32 disk_index,uint32 fs_index,uint32 inode_num)
{
	PtrArray<File>* files=new PtrArray<File>;
	*files=Disks::disks()[disk_index][fs_index].scanDir(inode_num);
	if(files->size()==0)
		return NULL;
	return files;
}
EXTFS_DLL_EXPORT PtrArray<File>* dll_scanDirByPath(uint32 disk_index,uint32 fs_index,const uint16* path)
{
	PtrArray<File>* files=new PtrArray<File>;
	*files=Disks::disks()[disk_index][fs_index].scanDir((const wint16*)path);
	if(files->size()==0)
		return NULL;
	return files;
}
EXTFS_DLL_EXPORT bool dll_isHaveSubDir(uint32 disk_index,uint32 fs_index,uint32 inode_num)
{
	return Disks::disks()[disk_index][fs_index].checkDir(inode_num);
}


EXTFS_DLL_EXPORT bool dll_copy(uint32 disk_index,uint32 fs_index,uint32 inode_num,const uint16* to_path)
{
	return Disks::disks()[disk_index][fs_index].copy(inode_num,(const wint16*)to_path);
}


EXTFS_DLL_EXPORT void dll_releaseFilePtrArray(PtrArray<File>* files){delete files;}

