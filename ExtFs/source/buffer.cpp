#include "../include/buffer.h"
bool Buffer::operator==(const Buffer& x) const
{
	if(data_size!=x.data_size)
		return false;
	for(uint32 i=0;i<data_size;i++)
		if(*(data_ptr+i)!=*(x.data_ptr+i))
			return false;
	return true;
}
void Buffer::operator=(const Buffer& x)
{
	delete[] data_ptr;
	data_ptr=x.data_ptr;
	data_size=x.data_size;
}
Buffer::~Buffer()
{
	if(is_destroy)
		delete[] data_ptr;
}

