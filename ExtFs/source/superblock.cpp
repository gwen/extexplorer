#include "../include/superblock.h"
#include "../include/type.h"
#include "../include/winapi.h"
#include "../include/macro_str.h"
const Buffer SuperBlock::fsName()
{
	int32 len=MultiByteToWideChar(CP_UTF8,0,(LPCSTR)EXTFS_NAME(block.data()),EXTFS_NAME_LENGTH,NULL,0);
	Buffer to(new uint8[len*4],len*4); 
	for(uint32 i=0;i<len*2;i++)
		to.data()[i]=0;
	MultiByteToWideChar(CP_UTF8,0,(LPCSTR)EXTFS_NAME(block.data()),EXTFS_NAME_LENGTH,(LPWSTR)to.data(),len);
	uint32 i=0;
	for(;i<len*2;i+=2)
		if(*(uint16*)(to.data()+i)>0x9fbf||*(uint16*)(to.data()+i)==0)
		{
			*(uint16*)(to.data()+i)=0;
			break;
		}
	*(uint16*)(to.data()+i)=SLASH;
	*(uint16*)(to.data()+i+2)='E';
	*(uint16*)(to.data()+i+4)='X';
	*(uint16*)(to.data()+i+6)='T';
	*(uint16*)(to.data()+i+8)=0;
	return to;
}